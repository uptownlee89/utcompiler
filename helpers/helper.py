

def println_r(list):
    for each in list:
        print each

def generate_type(types):
    for each in types:
        print each[1].upper() + "(" + "),"


def keywords_to_jflex(list):
    parts = []
    for each in list:
        parts.append("\"" + each + "\" {return sf.newSymbol(\"" + each.upper() + "\",sym." + each.upper() + "); }")
    return parts


def operators_to_jflex(list):
    parts = []
    for each in list:
        parts.append("\"" + each[0] + "\" {return sf.newSymbol(\"" + each[1].upper() + "\",sym." +
                       each[1].upper() + "); }")
    return parts


c_keywords = [
    # "auto",
    "break",
    "case",
    "char",
    # "const",
    # "continue",
    "default",
    "do",
    # "double",
    "else",
    # "enum",
    # "extern",
    "float",
    "for",
    # "goto",
    "if",
    "int",
    # "long",
    # "register",
    "return",
    # "short",
    # "signed",
    # "sizeof",
    # "static",
    # "struct",
    "switch",
    # "typedef",
    # "unary",
    # "unsigned",
    # "void",
    # "volatile",
    "while"
]

# unop : -
# binop : *, /, +, -, <, >, <=, >=, ==, !=
c_operators = [
    ("==", "EQUALS"),
    ("!=", "DIFF"),
    ("<=", "LTE"),
    (">=", "GTE"),
    ("<", "LT"),
    (">", "GT"),
    ("+", "PLUS"),
    ("*", "TIMES"),
    ("-", "MINUS"),
    ("/", "DIV"),


    # ("|", "OR"),
    # ("&", "AND"),
    # ("%", "MOD"),

    (",", "COMMA"),
    (";", "SEMICOLON"),
    ("=", "ASSIGN"),
    # ("!", "NOT"),


    ("(", "LPB"),
    (")", "RPB"),

    ("[", "LSB"),
    ("]", "RSB"),

    ("{", "LB"),
    ("}", "RB"),
]


println_r(keywords_to_jflex(c_keywords))
print ""
println_r(operators_to_jflex(c_operators))
print ""
print "terminal " + ", ".join([x.upper() for x in c_keywords] + [x[1].upper() for x in c_operators])
print ""
generate_type(c_operators)