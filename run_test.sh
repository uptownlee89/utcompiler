# /bin/sh
cd src/utmcc/
java -jar ../../bin/java-cup-11b.jar -package utmcc -parser Parser -symbols sym ../../cup/Parser.cup
cd ../../
java -jar bin/jflex-1.5.1.jar -d src/utmcc --noinputstreamctor flex/Scanner.jflex
rm -r classes
mkdir classes
javac -d classes/ -classpath lib/java-cup-11b-runtime.jar src/Main.java src/utmcc/*.java src/utmcc/*/*.java
rm -r dist
mkdir dist
jar cfm dist/Compiler.jar manifest.mf -C classes/ .
cp lib/java-cup-11b-runtime.jar dist/
java -jar dist/Compiler.jar tests/all.c tests/error.c tests/error.c tests/error1.c tests/error2.c tests/error3.c tests/error4.c tests/order_test.c tests/order_test2.c tests/order_test3.c