package utmcc;
import java_cup.runtime.ComplexSymbolFactory;
import java.util.ArrayList;
import utmcc.error.ScanError;
%%

%cup
%line
%column
%class Scanner
%unicode
%{
  public ArrayList<ScanError> errors = new ArrayList<ScanError>();
  private UTSymbolFactory sf;
  private Compiler compiler;
  public Scanner(java.io.InputStreamReader r, UTSymbolFactory sf, Compiler compiler){
    this(r);
    this.sf=sf;
    this.compiler = compiler;
    this.compiler.scanner = this;
    this.sf.compiler = this.compiler;
  }

  public int currentLine(){
    return yyline + 1;
  }

  public int currentColumn(){
    return yycolumn;
  }
  public ArrayList<ScanError> getErrors(){
    return this.errors;
  }
  
%}

%eofval{
  return sf.newSymbol("EOF", sym.EOF);
%eofval}

ALPHA = [a-zA-Z_]
ALPHA_NUM = {ALPHA}|[0-9]
IDENTIFIER = {ALPHA}({ALPHA_NUM})*

NEWLINE = \n | \u2028 | \u2029 | \u000B | \u000C | \u0085
SPACING = [ \t\r\f] | {NEWLINE}

%xstate MULTI_COMMENT, MONO_COMMENT

%%

<MONO_COMMENT> {
  {NEWLINE}         {yybegin(YYINITIAL);}
  .                 {}
}


<MULTI_COMMENT>{
  \*\/              {yybegin(YYINITIAL);}
  . | {NEWLINE}     {}
}

<YYINITIAL>{

  \/\/              {yybegin(MONO_COMMENT);}
  \/\*              {yybegin(MULTI_COMMENT);}

/* 오퍼레이터 */
"==" {return sf.newSymbol("EQUALS",sym.EQUALS, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"!=" {return sf.newSymbol("DIFF",sym.DIFF, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"<=" {return sf.newSymbol("LTE",sym.LTE, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
">=" {return sf.newSymbol("GTE",sym.GTE, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"<" {return sf.newSymbol("LT",sym.LT, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
">" {return sf.newSymbol("GT",sym.GT, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"+" {return sf.newSymbol("PLUS",sym.PLUS, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"*" {return sf.newSymbol("TIMES",sym.TIMES, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"-" {return sf.newSymbol("MINUS",sym.MINUS, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"/" {return sf.newSymbol("DIV",sym.DIV, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"," {return sf.newSymbol("COMMA",sym.COMMA, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
";" {return sf.newSymbol("SEMICOLON",sym.SEMICOLON, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
":" {return sf.newSymbol("COLON",sym.COLON, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"=" {return sf.newSymbol("ASSIGN",sym.ASSIGN, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
//"!" {return sf.newSymbol("NOT",sym.NOT); }
"(" {return sf.newSymbol("LPB",sym.LPB, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
")" {return sf.newSymbol("RPB",sym.RPB, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"[" {return sf.newSymbol("LSB",sym.LSB, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"]" {return sf.newSymbol("RSB",sym.RSB, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"{" {return sf.newSymbol("LB",sym.LB, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"}" {return sf.newSymbol("RB",sym.RB, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }


/* 키워드 */
"break" {return sf.newSymbol("BREAK",sym.BREAK, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"case" {return sf.newSymbol("CASE",sym.CASE, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"default" {return sf.newSymbol("DEFAULT",sym.DEFAULT, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"else" {return sf.newSymbol("ELSE",sym.ELSE, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"float" {return sf.newSymbol("FLOAT",sym.FLOAT, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"for" {return sf.newSymbol("FOR",sym.FOR, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"if" {return sf.newSymbol("IF",sym.IF, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"int" {return sf.newSymbol("INT",sym.INT, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
//"char" {return sf.newSymbol("CHAR",sym.CHAR); }
"do" {return sf.newSymbol("DO",sym.DO); }
"return" {return sf.newSymbol("RETURN",sym.RETURN, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"switch" {return sf.newSymbol("SWITCH",sym.SWITCH, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }
"while" {return sf.newSymbol("WHILE",sym.WHILE, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength())); }


{IDENTIFIER}  {  return sf.newSymbol("IDENTIFIER", sym.IDENTIFIER, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength()), new String(yytext()));   }

[0-9]+   {  return sf.newSymbol("INTEGER", sym.INTEGER, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength()), new Integer(yytext()));   }

[0-9]+\.[0-9]+    {  return sf.newSymbol("DOUBLE", sym.DOUBLE, new ComplexSymbolFactory.Location("line", yyline, yycolumn), new ComplexSymbolFactory.Location("line", yyline, yycolumn+yylength()), new Double(yytext()));   }


{SPACING}         {/* ignore white space. */ }

.                 { this.errors.add(new ScanError("Scanner Error", currentLine(), currentColumn(), yylength(), yytext()));
                    throw new java.io.IOException("Unexpected Character");
                                                                                }

}