int error;
int garray[10];

int init(){
	error = -1;
}

int fib(int x){
	if(x == 1) return 1;
	if(x == 0) return 1;
	return fib(x-1) + fib(x-2);
}
int fib_test(){
	if(fib(0) != 1) printf(error);
	if(fib(1) != 1) printf(error);
	if(fib(2) != 2) printf(error);
	if(fib(3) != 3) printf(error);
	if(fib(4) != 5) printf(error);
	if(fib(7) != 21) printf(error);

	{
		int i;
		int iter[20];
		iter[0] = 1;
		iter[1] = 1;
		for(i=2; i<20; i=i+1){
		    printf(i);
			iter[i] = iter[i-1] + iter[i-2];
			if(fib(i) != iter[i]){
				printf(error);
            }
		}
	}
}


int fac(int x){
	if(x < 1) return 1;
	return x * fac(x-1);
}
int fac_test(){
	int i;
	int iter[10];
	iter[0] = 1;
	for(i=1;i<10;i=i+1) iter[i] = iter[i-1] * i;
	for(i=0;i<10;i=i+1){
		if(iter[i] != fac(i))
			printf(error);
	}
}

int return_test2(){
	int i;
	for(i=0; 10; i=i+1)
		for(i=0; 10; i=i+1)
			return 1;
}
int return_test(){
	if(return_test2() > 1) printf(error);
	if(return_test2() < 1) printf(error);
}



int if_test(){
    int temp;
	scanf(garray[1]);
	scanf(temp);
	if(garray[1]!=temp) printf(error);

	if(1 > 2) printf(error);
	if(2 < 1) printf(error);
	if(-100 > 200) printf(error);
	if(-10 >= 20) printf(error);
	if(20 <= 10) printf(error);
	if(20 == 10) printf(error);
	if(10 != 10) printf(error);
	else	printf(1);
	if(printf(1) != 0) printf(error);

	if(10 < 20) printf(1);
	else printf(error);

	if(10 > 20) printf(error);
	else if(10 < -100) printf(error);
	else printf(1);
}


int while_test(){
	int i;
	i = 10;
	while(i < 10)
		printf(error);
}

int do_while_test(){
	int i;
	i = 10;
	do{
		i = 100;
	}while(i < 10);

	if(i != 100)
		printf(error);
}

int compount_stmt_test(){
	int a;
	a = 10;
	{
		int a;
		a = 20;
		{
			int a;
			a = 30;
			{
				int a[10];
				a[0] = 40;
				if(a[0] != 40)
					printf(error);
			}
			if(a != 30)
				printf(error);
		}
		if(a != 20)
			printf(error);
	}
}


int bubble_sort(int a[100], int size){
	int i, j;
	for(i=0;i<size;i=i+1){
		for(j=i+1;j<size;j=j+1){
			if(a[i] > a[j]){
				int tmp;
				tmp = a[i];
				a[i] = a[j];
				a[j] = tmp;
			}
		}
	}
}
int print_array(int a[10], int size){
	int i;
	for(i=0;i<size;i=i+1)
		printf(a[i]);
}

int local_sort_test(){
	int array[10];
    array[0] = 546;
    array[1] = 35;
    array[2] = 1;
    array[3] = 19;
    array[4] = 69;
    array[5] = 239;
    array[6] = 9;
    array[7] = 469;
    array[8] = 359;
    array[9] = 92;

    print_array(array, 10);
    bubble_sort(array, 10);
    printf(0);
    printf(0);
    print_array(array, 10);
}

int global_sort_test(){
    garray[0] = 546;
    garray[1] = 35;
    garray[2] = 1;
    garray[3] = 19;
    garray[4] = 69;
    garray[5] = 239;
    garray[6] = 9;
    garray[7] = 469;
    garray[8] = 359;
    garray[9] = 92;

    print_array(garray, 10);
    bubble_sort(garray, 10);
    printf(0);
    printf(0);
    print_array(garray, 10);
}


int get_array(int garray[100], int size){
	int i;
	for(i=0;i<size;i=i+1)
		scanf(garray[i]);

}
int bubsort_with_print(int a[100], int size){
	int i, j;
	get_array(a, size);
	print_array(a, size);
	for(i=0;i<size;i=i+1){
		for(j=i+1;j<size;j=j+1){
			if(a[i] > a[j]){
				int tmp;
				tmp = a[i];
				a[i] = a[j];
				a[j] = tmp;
			}
		}
	}
	printf(0);
	printf(0);
	print_array(a, size);
}

int switch_go(){
	int in;
	scanf(in);
	switch(in){
		case 1:
			fib_test();
			break;
		case 2:
			fac_test();
			break;
		case 3:
			return_test();
			break;
		case 4:
			if_test();
			break;
		case 5:
			while_test();
			break;
		case 6:
			do_while_test();
			break;
		case 7:
			compount_stmt_test();
			break;
		case 8:
			local_sort_test();
			break;
		case 9:
			global_sort_test();
			break;

		default:
			bubsort_with_print(garray, 10);
			break;

	}
}


int main(){
	init();
	switch_go();
}


