int array[300];
int tmpArray[300];
int N;
int SEED;
int ASSERT_FALSE;

int mod(int a, int b) {
  if(a > b) a = a-b*(a/b);
  return a;
}
int randInt() {
  SEED = SEED * SEED * SEED + SEED * SEED + SEED + 1;
  if(SEED < 0) SEED = -SEED;
  return SEED;
}
int swap(int i, int j) {
  int tmp;
  tmp = array[i];
  array[i] = array[j];
  array[j] = tmp;
  return 1;
}

int mergeSort_(int from, int to) {
  int mid, tmp, i, j, k;
  if(from > to) printf(ASSERT_FALSE);
  if(from == to) printf(ASSERT_FALSE);
  if(from + 1 == to) return 1;
  if(from + 2 == to) {
    if(array[from] > array[from+1]) swap(from, from+1);
    return 1;
  }
  mid = (from + to) / 2;
  mergeSort_(from, mid);
  mergeSort_(mid, to);
  i = from;
  j = mid;
  k = from;
  while((mid-i)*(to-j) > 0) {
    if(array[i] < array[j]) {
      tmpArray[k] = array[i];
      i = i+1;
    }
    else {
      tmpArray[k] = array[j];
      j = j+1;
    }
    k = k+1;
  }
  while(i < mid) {
    tmpArray[k] = array[i];
    i = i+1;
    k = k+1;
  }
  while(j < to) {
    tmpArray[k] = array[j];
    j = j+1;
    k = k+1;
  }
  if(k != to) printf(ASSERT_FALSE);
  for(i=from; i<to; i=i+1) array[i] = tmpArray[i];
  for(i=from; i<to-1; i=i+1) if(array[i] > array[i+1]) printf(ASSERT_FALSE);
  return 1;
}

int mergeSort() {
  return mergeSort_(0, N);
}

int main() {
  int i,x,y,sum;
  N = 137;
  ASSERT_FALSE = 999999;
  SEED = 672745;
  for(i=0; i<N; i=i+1) array[i] = i;
  for(i=0; i<111; i=i+1) {
    x = mod(randInt(),N);
    y = mod(randInt(),N);
    if(x < 0) printf(ASSERT_FALSE);
    if(x >= N) printf(ASSERT_FALSE);
    if(y < 0) printf(ASSERT_FALSE);
    if(y >= N) printf(ASSERT_FALSE);
    swap(x,y);
  }
  sum = 0;
  for(i=0; i<N; i=i+1) sum = sum + array[i];
  mergeSort();
  for(i=0; i<N; i=i+1) sum = sum - array[i];
  if(sum != 0) printf(ASSERT_FALSE);
  for(i=0; i<N; i=i+1) if(array[i] != i) printf(ASSERT_FALSE);
  printf(42);
  return 0;
}
