===========================
 Uptown Mini C Compiler 0.5
===========================

This project is for CS 420 in KAIST.

Author
======

Lee, Juyoung (lee.juyoung04@gmail.com)


Build Environment
=================

* Java 1.8

* Cup 0.11b & JFlex 1.5.1

* Ant 1.9.3

How to build
============

Using ANT
---------

.. Topic:: Cup only

   $ ant cup

.. Topic:: Cup & JFlex

   $ ant jflex

.. Topic:: Run

   $ ant run -Dtarget=your_c_code.c

.. Topic:: Clean

   $ ant clean


Without ANT
-----------

.. Topic:: Cup

   # Move to 'utmcc' package directory.

   $ cd src/utmcc/ 

   utmcc $ java -jar ../../bin/java-cup-11b.jar -package utmcc -parser Parser -symbols sym ../../cup/Parser.cup
   
   # Return to Project directory.

   utmcc $ cd ../../

.. Topic:: JFlex

   $ java -jar bin/jflex-1.5.1.jar -d src/utmcc --noinputstreamctor flex/Scanner.jflex


.. Topic:: Compile

   $ mkdir classes

   $ javac -d classes/ -classpath lib/java-cup-11b-runtime.jar src/Main.java src/utmcc/\*.java src/utmcc/\*/\*.java

.. Topic:: Merge & Copy Library

   $ mkdir dist

   $ jar cfm dist/Compiler.jar manifest.mf -C classes/ .

   $ cp lib/java-cup-11b-runtime.jar dist/

.. Topic:: Run

   $ java -jar dist/Compiler.jar your_c_code.c

Change Log
==========

6/19/2014
---------

* Version 0.5.0 released

* Can generating simple T machine code.

5/22/2014
---------

* Version 0.4.0 released

* Dirty validator ... T_T


5/2/2014
---------

* Version 0.3.0 released

* Can generate simple T machine code (tested with magic square program, example/magic.c)

5/2/2014
---------

* Version 0.2.1 released

* Project1-Step1 requirement added.


4/30/2014
---------

* Version 0.2 released

* print out Symbol tables and C-like tree.


4/14/2014
---------

* Version 0.1 released

* Just print out production roles and scanner's move

* Simple error handling

Scoring Record for Class
========================

* Version 0.1 25.0/30.0 (Output Spec was not matched.)

LICENSE
=======

Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.