/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * Created by uptown on 2014. 4. 5..
 */

import utmcc.Compiler;
// ref: https://code.google.com/p/my-c-compiler/
// ref: http://www.lysator.liu.se/c/ANSI-C-grammar-y.html#direct-declarator
// ref: http://www.lysator.liu.se/c/ANSI-C-grammar-l.html
// ref: https://github.com/periodic/Java-Calculator/blob/master/calculator.cup
// ref: http://www.tutorials4u.com/c/t90.htm
// ref: http://www2.cs.tum.edu/projects/cup/manual.html
// ref: http://jflex.de/manual.html
// ref: http://www.cinsk.org/gcc-error/gcc-error.html # gcc warnings / errors list

public class Main {

    public static void main(String[] args) {
        for (String filename : args) {
            try {
                System.out.println("Compile Start:" + filename);
                Compiler compiler = Compiler.compile(filename);
                if (compiler.getProgram().errors().isEmpty()) {
                    System.out.println("Compile Succeed:" + filename + "");
                } else {

                    System.out.flush();
                    for (utmcc.error.Error error : compiler.getProgram().errors()) {
                        System.err.printf("%s\n", error);
                    }
                    System.err.println("Compile Failed:" + filename + "");
                    System.err.flush();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            System.out.flush();
            System.err.flush();
        }
    }

}