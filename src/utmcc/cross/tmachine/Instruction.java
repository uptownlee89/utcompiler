/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc.cross.tmachine;

import com.sun.deploy.util.StringUtils;

import java.util.ArrayList;

/**
 * Created by uptown on 2014. 5. 9..
 */
public class Instruction {
    public String command;
    public boolean isIgnore = false;
    ArrayList<Operand> operands;

    public Instruction() {

    }

    public Instruction(String command, Operand operand0) {
        this.command = command;
        this.operands = new ArrayList<Operand>();
        this.operands.add(operand0);
        _init();
    }

    public Instruction(String command, Operand operand0, Operand operand1) {
        this.command = command;
        this.operands = new ArrayList<Operand>();
        this.operands.add(operand0);
        this.operands.add(operand1);
        _init();
    }

    public Instruction(String command, Operand operand0, Operand operand1, Operand operand2) {
        this.command = command;
        this.operands = new ArrayList<Operand>();
        this.operands.add(operand0);
        this.operands.add(operand1);
        this.operands.add(operand2);
        _init();
    }

    public void _init() {
//        if(command.equals("MOVE") && ((MemoryOperand)(operands.get(1))).identifier.equals("VR")){
//            if(operands.get(0) instanceof IntOperand) {
//                TCode.vrIndex.put(((IntOperand) (((MemoryOperand) (operands.get(1))).offset)).getValue(), ((IntOperand) operands.get(0)).getValue());
//                isIgnore = true;
//            }
//            else{
//                TCode.vrIndex.remove(((IntOperand) (((MemoryOperand) (operands.get(1))).offset)).getValue());
//            }
//        }
//        else if(operands.size() == 3 && ((MemoryOperand)(operands.get(2))).identifier.equals("VR")) {
//            TCode.vrIndex.remove(((IntOperand) (((MemoryOperand) (operands.get(2))).offset)).getValue());
//        }
//        else if(!command.startsWith("JM")){
//            ArrayList<Operand> newOperands = new ArrayList<Operand>();
//            for (Operand op : operands) {
//                if (op instanceof MemoryOperand && ((MemoryOperand) op).identifier.equals("VR")) {
//                    Integer val = (Integer) TCode.vrIndex.get(((IntOperand)(((MemoryOperand) op).offset)).getValue());
//                    if (val != null) {
//                        newOperands.add(new IntOperand(val));
//                    }
//                    else{
//                        newOperands.add(op);
//                    }
//                }
//                else{
//                    newOperands.add(op);
//                }
//            }
//            operands = newOperands;
//        }
//        else{
//            TCode.vrIndex.clear();
//        }
    }

    @Override
    public String toString() {
        String ret = "\t" + command;
        ArrayList<String> temp = new ArrayList<String>();

        for (Operand op : operands) {
            temp.add(op.toString());
        }

        ret += " " + StringUtils.join(temp, " ");
        return ret;
    }
}
