/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc.cross.tmachine;

/**
 * Created by uptown on 2014. 5. 9..
 */
public class MemoryOperand extends Operand {
    String identifier;
    Operand offset;
    boolean isValue;

    public MemoryOperand(String identifier, Operand offset) {
        this.identifier = identifier;
        this.offset = offset;
        isValue = false;
        if (identifier.equals("VR")) {
            TCode.usedRegister.add(new Integer(((IntOperand) offset).value));
            TCode.maxRegisterCnt = Math.max(TCode.maxRegisterCnt, ((IntOperand) offset).value);
        }
    }

    public MemoryOperand(String identifier, Operand offset, boolean isValue) {
        this.identifier = identifier;
        this.offset = offset;
        this.isValue = isValue;
        if (identifier.equals("VR")) {
            TCode.usedRegister.add(new Integer(((IntOperand) offset).value));
            TCode.maxRegisterCnt = Math.max(TCode.maxRegisterCnt, ((IntOperand) offset).value);
        }
    }

    public MemoryOperand(MemoryOperand operand, boolean isValue) {
        this.identifier = operand.identifier;
        this.offset = operand.offset;
        this.isValue = isValue;
    }

    public MemoryOperand(String identifier, int offset) {
        this.identifier = identifier;
        this.offset = new IntOperand(offset);
        isValue = false;
        if (identifier.equals("VR")) {
            TCode.usedRegister.add(new Integer(offset));
            TCode.maxRegisterCnt = Math.max(TCode.maxRegisterCnt, offset);
        }
    }

    public MemoryOperand(String identifier, int offset, boolean isValue) {
        this.identifier = identifier;
        this.offset = new IntOperand(offset);
        this.isValue = isValue;
        if (identifier.equals("VR")) {
            TCode.usedRegister.add(new Integer(offset));
            TCode.maxRegisterCnt = Math.max(TCode.maxRegisterCnt, offset);
        }
    }

    public MemoryOperand(String identifier) {
        this.identifier = identifier;
        this.offset = null;
        isValue = false;
    }

    public MemoryOperand(String identifier, boolean isValue) {
        this.identifier = identifier;
        this.offset = null;
        this.isValue = isValue;
    }
//
//    public MemoryOperand operandWithOffset(int offset) throws Exception {
//        if(this.offset instanceof IntOperand)
//            return new MemoryOperand(identifier, new IntOperand(((IntOperand) this.offset).getValue() + offset));
//        throw new Exception();
//    }

    @Override
    public String toString() {
        if (this.offset != null)
            return identifier + "(" + offset + ")" + (isValue ? "@" : "");
        return identifier + (isValue ? "@" : "");
    }
}
