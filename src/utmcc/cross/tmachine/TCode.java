/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc.cross.tmachine;

import utmcc.*;
import utmcc.expression.*;
import utmcc.statement.*;
import utmcc.token.BinaryOperator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by uptown on 2014. 5. 9..
 */
public class TCode {

    static public int maxRegisterCnt = 0;
    static public HashMap<Integer, Number> vrIndex = new HashMap<Integer, Number>();
    static public Set<Integer> usedRegister = new HashSet<Integer>();
    final private String sp = "SP";
    final private String fp = "FP";
    final private String register = "VR";
    final private String mem = "MEM";
    final private int frameInfoSize = 3;
    HashMap<String, MemoryOperand> globalVariableTable = new HashMap<String, MemoryOperand>();
    private ArrayList<String> areaList;
    private Program program;
    private int autoInc = 0;
    private int currentValStack = 0;

    public TCode(Program program){
        areaList = new ArrayList<String>();
        areaList.add(mem);
        areaList.add(register);
        areaList.add(fp);
        areaList.add(sp);
        this.program = program;
    }

    public String newLabel(String type){return type + autoInc++;}


    private ArrayList<Instruction> functionToCode(Function function) throws Exception {
        ArrayList<Instruction> instructions = new ArrayList<Instruction>();
        instructions.add(new LabInstruction("F_" + function.getIdentifier()));
        Block fb = function.getStatement().getBlock();
        ArrayList<SymbolTableEntry> st = fb.getSymbolTable();
        HashMap<String, MemoryOperand> variableTable = new HashMap<String, MemoryOperand>();
        currentValStack = 0;
        int paramForVal = frameInfoSize;
        for(SymbolTableEntry entry : st){
            if(entry.getRole() == SymbolTableEntry.VARIABLE) {
                Expression index = entry.getVariable().getIndex();
                if (entry.getType().getType() == sym.INT) {
                    if (index == null) {
                        variableTable.put(entry.getVariable().getIdentifier(), new MemoryOperand("FP@", new IntOperand(currentValStack + 1)));
                        currentValStack++;
                    } else {
                        if (!(index instanceof IntegerValue))
                            throw new Exception();
                        int cnt = ((IntegerValue) index).getValue();
                        ArrayMemoryOperand temp = new ArrayMemoryOperand("FP@", new IntOperand(currentValStack + 1));
                        temp.cnt = cnt;
                        variableTable.put(entry.getVariable().getIdentifier(), temp);
                        currentValStack += ((IntegerValue) index).getValue();
                    }
                } else {
                    if (index == null) {
                        variableTable.put(entry.getVariable().getIdentifier(), new FloatMemoryOperand("FP@", new IntOperand(currentValStack + 1)));
                        currentValStack++;
                    } else {
                        if (!(index instanceof IntegerValue))
                            throw new Exception();
                        int cnt = ((IntegerValue) index).getValue();
                        FloatArrayMemoryOperand temp = new FloatArrayMemoryOperand("FP@", new IntOperand(currentValStack + 1));
                        temp.cnt = cnt;
                        variableTable.put(entry.getVariable().getIdentifier(), temp);
                        currentValStack += ((IntegerValue) index).getValue();
                    }
                }
            }
            else{
                Expression index = entry.getVariable().getIndex();
                if (entry.getType().getType() == sym.INT) {
                    if (index == null) {
                        variableTable.put(entry.getVariable().getIdentifier(), new MemoryOperand("FP@", new IntOperand(-paramForVal)));
                        paramForVal++;
                    } else {
                        if (!(index instanceof IntegerValue))
                            throw new Exception();
                        int cnt = ((IntegerValue) index).getValue();
                        ArrayMemoryOperand temp = new ArrayMemoryOperand("FP@", new IntOperand(-paramForVal - cnt + 1));
                        temp.cnt = cnt;
                        variableTable.put(entry.getVariable().getIdentifier(), temp);
                        paramForVal += ((IntegerValue) index).getValue();
                    }
                } else {
                    if (index == null) {
                        variableTable.put(entry.getVariable().getIdentifier(), new FloatMemoryOperand("FP@", new IntOperand(-paramForVal)));
                        paramForVal++;
                    } else {
                        if (!(index instanceof IntegerValue))
                            throw new Exception();
                        int cnt = ((IntegerValue) index).getValue();
                        FloatArrayMemoryOperand temp = new FloatArrayMemoryOperand("FP@", new IntOperand(-paramForVal - cnt + 1));
                        temp.cnt = cnt;
                        variableTable.put(entry.getVariable().getIdentifier(), temp);
//                        variableTable.put(entry.getVariable().getIdentifier(), new FloatArrayMemoryOperand("FP@", new IntOperand(-paramForVal)));
                        paramForVal += ((IntegerValue) index).getValue();
                    }
                }
            }
        }
        vrIndex = new HashMap<Integer, Number>();
        usedRegister = new HashSet<Integer>();
        TCode.maxRegisterCnt = 0;
        CompoundStatement fs = function.getStatement();
        ArrayList<Instruction> temp = new ArrayList<Instruction>();
        for(Statement stat : fs.getStatements()){
            temp.addAll(statementToCode(variableTable, stat, 0));
        }
        if (currentValStack > 0)
            instructions.add(new Instruction("MOVE", new MemoryOperand("FP@", currentValStack), new MemoryOperand("SP")));
        else {
            instructions.add(new Instruction("MOVE", new MemoryOperand("FP@"), new MemoryOperand("SP")));
        }
        instructions.addAll(temp);
        //TODO 리턴타입 중복일수 있음
        instructions.add(new Instruction("JMP", new MemoryOperand("FP@", -2, true)));
        return instructions;
    }

    private ArrayList<Instruction> expressionToCode(HashMap<String, MemoryOperand> variableTable, Expression expression, int register) throws Exception {
        ArrayList<Instruction> instructions = new ArrayList<Instruction>();
        if(expression instanceof UnaryExpression){
            if(((UnaryExpression) expression).getOperator().getType() != sym.MINUS)
                throw new Exception();
            instructions.addAll(expressionToCode(variableTable, ((UnaryExpression) expression).getE(), register + 1));
            instructions.add(new Instruction("MUL", new IntOperand(-1), new MemoryOperand("VR", register + 1, true), new MemoryOperand("VR", register)));
        }
        else if(expression instanceof BracketExpression){
            instructions.addAll(expressionToCode(variableTable, ((BracketExpression) expression).getExpression(), register));
            if (((BracketExpression) expression).getCasted() == sym.INT) {
                instructions.add(new Instruction("F2I", new MemoryOperand("VR", register, true), new MemoryOperand("VR", register)));
            } else if (((BracketExpression) expression).getCasted() == sym.FLOAT) {
                instructions.add(new Instruction("I2F", new MemoryOperand("VR", register, true), new FloatMemoryOperand("VR", register)));
            }
        }
        else if(expression instanceof BinaryExpression){
            instructions.addAll(expressionToCode(variableTable, ((BinaryExpression) expression).getE0(), register + 2));
            instructions.add(new Instruction("MOVE", new MemoryOperand("VR", register + 2, true), new MemoryOperand("VR", register)));
            instructions.addAll(expressionToCode(variableTable, ((BinaryExpression) expression).getE1(), register + 1));
            if (((BinaryExpression) expression).getE0().expectedType() != ((BinaryExpression) expression).getE1().expectedType())
                throw new Exception();
            boolean isF = ((BinaryExpression) expression).getE0().expectedType() == sym.FLOAT;
            switch (((BinaryExpression) expression).getOperator().getType()){
                case sym.DIV:
                    instructions.add(new Instruction(isF ? "FDIV" : "DIV", new MemoryOperand("VR", register, true), new MemoryOperand("VR", register + 1, true), new MemoryOperand("VR", register)));
                    break;
                case sym.MINUS:
                    instructions.add(new Instruction(isF ? "FSUB" : "SUB", new MemoryOperand("VR", register, true), new MemoryOperand("VR", register + 1, true), new MemoryOperand("VR", register)));
                    break;
                case sym.PLUS:
                    instructions.add(new Instruction(isF ? "FADD" : "ADD", new MemoryOperand("VR", register, true), new MemoryOperand("VR", register + 1, true), new MemoryOperand("VR", register)));
                    break;
                case sym.TIMES:
                    instructions.add(new Instruction(isF ? "FMUL" : "MUL", new MemoryOperand("VR", register, true), new MemoryOperand("VR", register + 1, true), new MemoryOperand("VR", register)));
                    break;
                case sym.LT: {
                    String lab = newLabel("LT");
                    String labEnd = newLabel("LT_END");
                    instructions.add(new Instruction(isF ? "FSUB" : "SUB", new MemoryOperand("VR", register, true), new MemoryOperand("VR", register + 1, true), new MemoryOperand("VR", register)));
                    instructions.add(new Instruction("JMPN", new MemoryOperand("VR", register, true), new MemoryOperand(lab)));
                    instructions.add(new Instruction("MOVE", new IntOperand(0), new MemoryOperand("VR", register)));
                    instructions.add(new Instruction("JMP", new MemoryOperand(labEnd)));
                    instructions.add(new LabInstruction(lab));
                    instructions.add(new Instruction("MOVE", new IntOperand(1), new MemoryOperand("VR", register)));
                    instructions.add(new LabInstruction(labEnd));
                }
                    break;
                case sym.LTE:
                {
                    String lab = newLabel("LTE");
                    String labEnd = newLabel("LTE_END");
                    instructions.add(new Instruction(isF ? "FSUB" : "SUB", new MemoryOperand("VR", register, true), new MemoryOperand("VR", register + 1, true), new MemoryOperand("VR", register)));
                    instructions.add(new Instruction("JMPN", new MemoryOperand("VR", register, true), new MemoryOperand(lab)));
                    instructions.add(new Instruction("JMPZ", new MemoryOperand("VR", register, true), new MemoryOperand(lab)));
                    instructions.add(new Instruction("MOVE", new IntOperand(0), new MemoryOperand("VR", register)));
                    instructions.add(new Instruction("JMP", new MemoryOperand(labEnd)));
                    instructions.add(new LabInstruction(lab));
                    instructions.add(new Instruction("MOVE", new IntOperand(1), new MemoryOperand("VR", register)));
                    instructions.add(new LabInstruction(labEnd));
                }
                    break;
                case sym.EQUALS:
                {
                    String lab = newLabel("EQUALS");
                    String labEnd = newLabel("EQUALS_END");
                    instructions.add(new Instruction(isF ? "FSUB" : "SUB", new MemoryOperand("VR", register, true), new MemoryOperand("VR", register + 1, true), new MemoryOperand("VR", register)));
                    instructions.add(new Instruction("JMPZ", new MemoryOperand("VR", register, true), new MemoryOperand(lab)));
                    instructions.add(new Instruction("MOVE", new IntOperand(0), new MemoryOperand("VR", register)));
                    instructions.add(new Instruction("JMP", new MemoryOperand(labEnd)));
                    instructions.add(new LabInstruction(lab));
                    instructions.add(new Instruction("MOVE", new IntOperand(1), new MemoryOperand("VR", register)));
                    instructions.add(new LabInstruction(labEnd));
                }
                    break;
                case sym.DIFF:
                {
                    String lab = newLabel("DIFF");
                    String labEnd = newLabel("DIFF_END");
                    instructions.add(new Instruction(isF ? "FSUB" : "SUB", new MemoryOperand("VR", register, true), new MemoryOperand("VR", register + 1, true), new MemoryOperand("VR", register)));
                    instructions.add(new Instruction("JMPZ", new MemoryOperand("VR", register, true), new MemoryOperand(lab)));
                    instructions.add(new Instruction("MOVE", new IntOperand(1), new MemoryOperand("VR", register)));
                    instructions.add(new Instruction("JMP", new MemoryOperand(labEnd)));
                    instructions.add(new LabInstruction(lab));
                    instructions.add(new Instruction("MOVE", new IntOperand(0), new MemoryOperand("VR", register)));
                    instructions.add(new LabInstruction(labEnd));
                }
                    break;
                case sym.GT: {
                    String lab = newLabel("GT");
                    String labEnd = newLabel("GT_END");
                    instructions.add(new Instruction(isF ? "FSUB" : "SUB", new MemoryOperand("VR", register + 1, true), new MemoryOperand("VR", register, true), new MemoryOperand("VR", register)));
                    instructions.add(new Instruction("JMPN", new MemoryOperand("VR", register, true), new MemoryOperand(lab)));
                    instructions.add(new Instruction("MOVE", new IntOperand(0), new MemoryOperand("VR", register)));
                    instructions.add(new Instruction("JMP", new MemoryOperand(labEnd)));
                    instructions.add(new LabInstruction(lab));
                    instructions.add(new Instruction("MOVE", new IntOperand(1), new MemoryOperand("VR", register)));
                    instructions.add(new LabInstruction(labEnd));
                }
                break;
                case sym.GTE: {
                    String lab = newLabel("GTE");
                    String labEnd = newLabel("GTE_END");
                    instructions.add(new Instruction(isF ? "FSUB" : "SUB", new MemoryOperand("VR", register + 1, true), new MemoryOperand("VR", register, true), new MemoryOperand("VR", register)));
                    instructions.add(new Instruction("JMPN", new MemoryOperand("VR", register, true), new MemoryOperand(lab)));
                    instructions.add(new Instruction("JMPZ", new MemoryOperand("VR", register, true), new MemoryOperand(lab)));
                    instructions.add(new Instruction("MOVE", new IntOperand(0), new MemoryOperand("VR", register)));
                    instructions.add(new Instruction("JMP", new MemoryOperand(labEnd)));
                    instructions.add(new LabInstruction(lab));
                    instructions.add(new Instruction("MOVE", new IntOperand(1), new MemoryOperand("VR", register)));
                    instructions.add(new LabInstruction(labEnd));
                }
                break;
                //TODO 비교식
                default:
                    System.out.println("**"+expression);
            }
        }
        else if(expression instanceof IntegerValue){
            instructions.add(new Instruction("MOVE", new IntOperand(((IntegerValue) expression).getValue()), new MemoryOperand("VR", register)));
        }
        else if(expression instanceof FloatValue){
            instructions.add(new Instruction("MOVE", new FloatOperand(((FloatValue) expression).getValue()), new FloatMemoryOperand("VR", register)));
        }
        else if(expression instanceof Variable){
            Variable var = (Variable)expression;
            Expression index = var.getIndex();
            MemoryOperand vp = variableTable.get(var.getIdentifier());
            if (vp == null)
                vp = globalVariableTable.get(var.getIdentifier());
            instructions.add(new Comment("variable " + expression));
            if(index != null){
                instructions.addAll(expressionToCode(variableTable, index, register + 1));
                instructions.add(new Instruction("ADD", new MemoryOperand("VR", register + 1, true), new MemoryOperand(vp.offset.toString()), new MemoryOperand("VR", register + 1)));
//                System.out.println(expression);
//                System.out.println(vp);
                //instructions.add(new Instruction("WRITE", new MemoryOperand("VR", register + 1, true)));
                if (var.expectedType() == sym.INT) {
                    instructions.add(new Instruction("MOVE", new MemoryOperand(vp.identifier, new MemoryOperand("VR", register + 1, true), true), new MemoryOperand("VR", register)));
                } else {
                    instructions.add(new Instruction("MOVE", new FloatMemoryOperand(vp.identifier, new MemoryOperand("VR", register + 1, true), true), new FloatMemoryOperand("VR", register)));
                }

            }
            else {
                MemoryOperand vp2 = variableTable.get(((Variable) expression).getIdentifier());
                if (vp2 == null)
                    vp2 = globalVariableTable.get(((Variable) expression).getIdentifier());
                if (var.expectedType() == sym.INT) {
                    instructions.add(new Instruction("MOVE", new MemoryOperand(vp2, true), new MemoryOperand("VR", register)));
                } else {
                    instructions.add(new Instruction("MOVE", new FloatMemoryOperand(vp2, true), new FloatMemoryOperand("VR", register)));
                }
            }
            if (((Variable) expression).getCasted() == sym.INT) {
                instructions.add(new Instruction("F2I", new MemoryOperand("VR", register, true), new MemoryOperand("VR", register)));
            } else if (((Variable) expression).getCasted() == sym.FLOAT) {
                instructions.add(new Instruction("I2F", new MemoryOperand("VR", register, true), new FloatMemoryOperand("VR", register)));
            }
            instructions.add(new Comment("//end variable " + expression));
        }
        else if(expression instanceof FunctionCall){
            FunctionCall fc = (FunctionCall)expression;
            ArrayList<Expression> args = fc.getArguments();


            //TODO 배열 Arguments 처리
            if (fc.getIdentifier().equals("scanf")) {

                int newFPOffset = frameInfoSize + 1;
                instructions.add(new Instruction("MOVE", new MemoryOperand("FP", true), new MemoryOperand("SP@", newFPOffset)));

                MemoryOperand vp = variableTable.get(((Variable) args.get(0)).getIdentifier());
                if (vp == null)
                    vp = globalVariableTable.get(((Variable) args.get(0)).getIdentifier());
                Expression index = ((Variable) args.get(0)).getIndex();
                if (index != null) {
                    instructions.addAll(expressionToCode(variableTable, index, register));
                    instructions.add(new Instruction("ADD", new MemoryOperand("VR", register, true), new MemoryOperand(vp.offset.toString()), new MemoryOperand("VR", register)));
                    instructions.add(new Instruction("MOVE", new MemoryOperand(vp.identifier, new MemoryOperand("VR", register, true)), new MemoryOperand("SP@", 1)));
                } else {
                    instructions.add(new Instruction("MOVE", vp, new MemoryOperand("SP@", 1)));
                }
                instructions.add(new Instruction("MOVE", new MemoryOperand("SP@", newFPOffset), new MemoryOperand("FP")));
                String lab = newLabel("ENDFUNC");
                instructions.add(new Instruction("MOVE", new MemoryOperand(lab), new MemoryOperand("FP@", -2)));
                instructions.add(new Instruction("JMP", new MemoryOperand("F_" + fc.getIdentifier())));
                instructions.add(new LabInstruction(lab));
                instructions.add(new Instruction("MOVE", new MemoryOperand("FP@", -1, true), new MemoryOperand("VR", register)));
                instructions.add(new Instruction("MOVE", new MemoryOperand("FP@", -newFPOffset), new MemoryOperand("SP")));
                instructions.add(new Instruction("MOVE", new MemoryOperand("FP@", true), new MemoryOperand("FP")));
            } else {
//                            Expression index = var.getIndex();
//                            if(vp == null)
//                                vp = globalVariableTable.get(var.getIdentifier());
//                            instructions.add(new Comment("//variable " + expression));
//                            if(index != null){
//                                instructions.addAll(expressionToCode(variableTable, index, register + 2));
//                                instructions.add(new Instruction("ADD", new MemoryOperand("VR", register + 2, true), new MemoryOperand(vp.offset.toString()), new MemoryOperand("VR", register + 2)));
//                                if(var.expectedType() == sym.INT){
//                                    instructions.add(new Instruction("MOVE", new MemoryOperand(vp.identifier, new MemoryOperand("VR", register + 2, true), true), new MemoryOperand("VR", register + 1)));
//                                }
//                                else{
//                                    instructions.add(new Instruction("MOVE", new FloatMemoryOperand(vp.identifier, new MemoryOperand("VR", register + 2, true), true), new FloatMemoryOperand("VR", register + 1)));
//                                }
//
//                            }
//                            else {
//                                MemoryOperand vp2 = variableTable.get(((Variable) expression).getIdentifier());
//                                if(vp2 == null)
//                                    vp2 = globalVariableTable.get(((Variable) expression).getIdentifier());
//                                if(var.expectedType() == sym.INT){
//                                    instructions.add(new Instruction("MOVE", new MemoryOperand(vp2, true), new MemoryOperand("VR", register + 1)));
//                                }
//                                else {
//                                    instructions.add(new Instruction("MOVE", new FloatMemoryOperand(vp2, true), new FloatMemoryOperand("VR", register + 1)));
//                                }
//                            }
//                            if(((Variable) expression).getCasted() == sym.INT){
//                                instructions.add(new Instruction("F2I", new MemoryOperand("VR", register + 1, true), new MemoryOperand("VR", register + 1)));
//                            }
//                            else if(((Variable) expression).getCasted() == sym.FLOAT){
//                                instructions.add(new Instruction("I2F", new MemoryOperand("VR", register + 1, true), new FloatMemoryOperand("VR", register + 1)));
//                            }
//                            instructions.add(new Comment("//end variable " + expression));
//                            instructions.add(new Instruction("MOVE", new MemoryOperand("VR", register + 1, true), new MemoryOperand("SP@", i)));
//                            i--;

                ArrayList<Instruction> temp = new ArrayList<Instruction>();
                HashSet<Integer> ur = new HashSet<Integer>(usedRegister);
                int len = 0;
                if (args != null) {
                    for (Expression e : args) {

                        if (e instanceof Variable && ((Variable) e).getIndex() == null) {

                            Variable var = (Variable) e;
                            MemoryOperand vp = variableTable.get(var.getIdentifier());
                            if (vp == null)
                                vp = globalVariableTable.get(var.getIdentifier());
                            if (vp instanceof ArrayMemoryOperand) {
                                len += ((ArrayMemoryOperand) vp).cnt;
                            } else if (vp instanceof FloatArrayMemoryOperand) {
                                len += ((FloatArrayMemoryOperand) vp).cnt;
                            } else {
                                len++;
                            }
                        } else {
                            len++;
                        }
                    }
                }
                if (args != null) {
                    int i = len;
                    for (Expression e : args) {
                        if (e instanceof Variable && ((Variable) e).getIndex() == null) {

                            Variable var = (Variable) e;
                            MemoryOperand vp = variableTable.get(var.getIdentifier());
                            if (vp == null)
                                vp = globalVariableTable.get(var.getIdentifier());
                            if (vp instanceof ArrayMemoryOperand) {
                                int cnt = ((ArrayMemoryOperand) vp).cnt;
                                for (int j = cnt - 1; j >= 0; j--) {
                                    temp.addAll(expressionToCode(variableTable, new Variable(var.getIdentifier(), new IntegerValue(j), sym.INT), register + 1));
                                    i--;
                                }

                            } else if (vp instanceof FloatArrayMemoryOperand) {
                                int cnt = ((FloatArrayMemoryOperand) vp).cnt;
                                for (int j = cnt - 1; j >= 0; j--) {
                                    temp.addAll(expressionToCode(variableTable, new Variable(var.getIdentifier(), new IntegerValue(j), sym.FLOAT), register + 1));
                                    i--;
                                }
                            } else {
                                temp.addAll(expressionToCode(variableTable, e, register + 1));
                                i--;
                            }
                        } else {
                            expressionToCode(variableTable, e, register + 1);
                            i--;
                        }
                    }
                }
                HashSet<Integer> oldUr = ur;
                ur = new HashSet<Integer>(usedRegister);

                temp = new ArrayList<Instruction>();
                int urSize = 0;
                for (Integer i : ur) {
                    if (i != register && vrIndex.get(i) == null)
                        urSize++;
                }
                int newFPOffset = frameInfoSize + len + urSize;
                if (args != null) {
                    int i = len + urSize;
                    for (Expression e : args) {
                        if (e instanceof Variable && ((Variable) e).getIndex() == null) {

                            Variable var = (Variable) e;
                            MemoryOperand vp = variableTable.get(var.getIdentifier());
                            if (vp == null)
                                vp = globalVariableTable.get(var.getIdentifier());
                            if (vp instanceof ArrayMemoryOperand) {
                                int cnt = ((ArrayMemoryOperand) vp).cnt;
                                for (int j = cnt - 1; j >= 0; j--) {
                                    temp.addAll(expressionToCode(variableTable, new Variable(var.getIdentifier(), new IntegerValue(j), sym.INT), register + 1));
                                    temp.add(new Instruction("MOVE", new MemoryOperand("VR", register + 1, true), new MemoryOperand("SP@", i)));
                                    i--;
                                }

                            } else if (vp instanceof FloatArrayMemoryOperand) {
                                int cnt = ((FloatArrayMemoryOperand) vp).cnt;
                                for (int j = cnt - 1; j >= 0; j--) {
                                    temp.addAll(expressionToCode(variableTable, new Variable(var.getIdentifier(), new IntegerValue(j), sym.FLOAT), register + 1));
                                    temp.add(new Instruction("MOVE", new MemoryOperand("VR", register + 1, true), new MemoryOperand("SP@", i)));
                                    i--;
                                }
                            } else {
                                temp.addAll(expressionToCode(variableTable, e, register + 1));
                                temp.add(new Instruction("MOVE", new MemoryOperand("VR", register + 1, true), new MemoryOperand("SP@", i)));
                                i--;
                            }
                        } else {
                            temp.addAll(expressionToCode(variableTable, e, register + 1));
                            temp.add(new Instruction("MOVE", new MemoryOperand("VR", register + 1, true), new MemoryOperand("SP@", i)));
                            i--;
                        }
                    }
                }
                instructions.addAll(temp);
                int k = 0;
                for (Integer i : ur) {
                    if (i != register && vrIndex.get(i) == null) {
                        instructions.add(new Instruction("MOVE", new MemoryOperand("VR", i, true), new MemoryOperand("SP@", k + 1)));
                        k++;
                    }
                }
                instructions.add(new Comment("push FP"));
                instructions.add(new Instruction("MOVE", new MemoryOperand("FP", true), new MemoryOperand("SP@", newFPOffset)));
                instructions.add(new Comment("push values"));
                instructions.add(new Instruction("MOVE", new MemoryOperand("SP@", newFPOffset), new MemoryOperand("FP")));
//            ret += "    MOVE SP@(" + newFPOffset + ") FP\n";
                String lab = newLabel("ENDFUNC");
                instructions.add(new Instruction("MOVE", new MemoryOperand(lab), new MemoryOperand("FP@", -2)));
                instructions.add(new Instruction("JMP", new MemoryOperand("F_" + fc.getIdentifier())));
                instructions.add(new LabInstruction(lab));
                instructions.add(new Instruction("MOVE", new MemoryOperand("FP@", -1, true), new MemoryOperand("VR", register)));
                instructions.add(new Instruction("MOVE", new MemoryOperand("FP@", -newFPOffset), new MemoryOperand("SP")));
                instructions.add(new Instruction("MOVE", new MemoryOperand("FP@", true), new MemoryOperand("FP")));
                k = 0;
                for (Integer i : ur) {
                    if (i != register && vrIndex.get(i) == null) {
                        instructions.add(new Instruction("MOVE", new MemoryOperand("SP@", k + 1, true), new MemoryOperand("VR", i)));
                        k++;
                    }
                }
            }
            if (((FunctionCall) expression).getCasted() == sym.INT) {
                instructions.add(new Instruction("F2I", new MemoryOperand("VR", register, true), new MemoryOperand("VR", register)));
            } else if (((FunctionCall) expression).getCasted() == sym.FLOAT) {
                instructions.add(new Instruction("I2F", new MemoryOperand("VR", register, true), new FloatMemoryOperand("VR", register)));
            }
        }
        else{
            System.out.println("***"+expression);
        }
        return instructions;
    }

    private ArrayList<Instruction> statementToCode(HashMap<String, MemoryOperand> variableTable, Statement statement, int register) throws Exception {
        ArrayList<Instruction> instructions = new ArrayList<Instruction>();
        if(statement instanceof CompoundStatement){
            CompoundStatement statement1 = (CompoundStatement)statement;
            //TODO define하는 부분
            HashMap<String, MemoryOperand> newVariableTable = (HashMap<String, MemoryOperand>) variableTable.clone();

            for (SymbolTableEntry entry : statement1.getBlock().getSymbolTable()) {
                if (entry.getRole() == SymbolTableEntry.VARIABLE) {
                    Expression index = entry.getVariable().getIndex();
                    newVariableTable.put(entry.getVariable().getIdentifier(), new MemoryOperand("FP@", new IntOperand(currentValStack + 1)));
                    if (index == null) {
                        currentValStack++;
                    } else {
                        if (!(index instanceof IntegerValue))
                            throw new Exception();
                        currentValStack += ((IntegerValue) index).getValue();
                    }
                } else {
                    throw new Exception();
                }
            }
            ArrayList<Statement> sl = statement1.getStatements();
            for(Statement s : sl){
                instructions.addAll(statementToCode(newVariableTable, s, register));
            }
        }
        else if(statement instanceof ReturnStatement){

            Expression expression = ((ReturnStatement) statement).getExpression();
            if(expression != null){
                if(expression instanceof IntegerValue || expression instanceof FloatValue){
                    instructions.add(new Instruction("MOVE", new MemoryOperand(expression.toString()), new MemoryOperand("FP@", -1)));
                }
                else{
                    instructions.addAll(expressionToCode(variableTable, expression, register));
                    instructions.add(new Instruction("MOVE", new MemoryOperand("VR", register, true), new MemoryOperand("FP@", -1)));
                }
            }
            else{
                instructions.add(new Instruction("MOVE", new IntOperand(0), new MemoryOperand("FP@", -1)));
            }
            instructions.add(new Instruction("JMP", new MemoryOperand("FP@", -2, true)));
        }
        else if(statement instanceof AssignStatement){
            Assign assign = ((AssignStatement) statement).getAssign();
            instructions.addAll(expressionToCode(variableTable, assign.getExpression(), register));
            Expression index = assign.getIndex();
            MemoryOperand vp = variableTable.get(assign.getIdentifier());
            if (vp == null)
                vp = globalVariableTable.get(assign.getIdentifier());
            instructions.add(new Comment("assign " + statement));
            if(index != null){
                instructions.addAll(expressionToCode(variableTable, index, register + 1));
                instructions.add(new Instruction("ADD", new MemoryOperand("VR", register + 1, true), new MemoryOperand(vp.offset.toString()), new MemoryOperand("VR", register + 1)));
                instructions.add(new Instruction("MOVE", new MemoryOperand("VR", register, true), new MemoryOperand(vp.identifier, new MemoryOperand("VR", register + 1, true))));
            }
            else{
                instructions.add(new Instruction("MOVE", new MemoryOperand("VR", register, true), vp));
            }
            instructions.add(new Comment("end assign"));
        }
        else if(statement instanceof ForStatement){
            ForStatement forStatement = (ForStatement)statement;
            String loopLabel = newLabel("FOR");
            String endLabel = newLabel("ENDFOR");
            {
                Assign assign = forStatement.getInitAssign();
                MemoryOperand vp = variableTable.get(assign.getIdentifier());
                if (vp == null)
                    vp = globalVariableTable.get(assign.getIdentifier());
                Expression index = assign.getIndex();
                instructions.addAll(expressionToCode(variableTable, assign.getExpression(), register));

                if (index != null) {
                    instructions.addAll(expressionToCode(variableTable, index, register + 1));
                    instructions.add(new Instruction("ADD", new MemoryOperand("VR", register + 1, true), new MemoryOperand(vp.offset.toString()), new MemoryOperand("VR", register + 1)));
                    instructions.add(new Instruction("MOVE", new MemoryOperand("VR", register, true), new MemoryOperand(vp.identifier, new MemoryOperand("VR", register + 1, true))));
                } else {
                    instructions.add(new Instruction("MOVE", new MemoryOperand("VR", register, true), vp));
                }
            }

            instructions.add(new LabInstruction(loopLabel));
            instructions.addAll(expressionToCode(variableTable, forStatement.getExpression(), register));
            instructions.add(new Instruction("JMPZ", new MemoryOperand("VR", register, true), new MemoryOperand(endLabel)));


            instructions.addAll(statementToCode(variableTable, forStatement.getStatement(), register + 1));

            {

                Assign assign = forStatement.getLoopAssign();
                MemoryOperand vp = variableTable.get(assign.getIdentifier());
                if (vp == null)
                    vp = globalVariableTable.get(assign.getIdentifier());
                Expression index = assign.getIndex();
                instructions.addAll(expressionToCode(variableTable, assign.getExpression(), register));

                if (index != null) {
                    instructions.addAll(expressionToCode(variableTable, index, register + 1));
                    instructions.add(new Instruction("ADD", new MemoryOperand("VR", register + 1, true), new MemoryOperand(vp.offset.toString()), new MemoryOperand("VR", register + 1)));
                    instructions.add(new Instruction("MOVE", new MemoryOperand("VR", register, true), new MemoryOperand(vp.identifier, new MemoryOperand("VR", register + 1, true))));
                } else {
                    instructions.add(new Instruction("MOVE", new MemoryOperand("VR", register, true), vp));
                }
            }

            instructions.add(new Instruction("JMP", new MemoryOperand(loopLabel)));
            instructions.add(new LabInstruction(endLabel));
        }
        else if(statement instanceof IfElseStatement){
            IfElseStatement s = (IfElseStatement)statement;
            instructions.addAll(expressionToCode(variableTable, s.getExpression(), register));
            String endLabel = newLabel("ENDIF");
            if(s.getElseStatement() != null){
                String elseLabel = newLabel("ELSE");
                instructions.add(new Instruction("JMPZ", new MemoryOperand("VR", register, true), new MemoryOperand(elseLabel)));
                instructions.addAll(statementToCode(variableTable, s.getIfStatement(), register));
                instructions.add(new Instruction("JMP", new MemoryOperand(endLabel)));
                instructions.add(new LabInstruction(elseLabel));
                instructions.addAll(statementToCode(variableTable, s.getElseStatement(), register));
                instructions.add(new LabInstruction(endLabel));
            }
            else{
                instructions.add(new Instruction("JMPZ", new MemoryOperand("VR", register, true), new MemoryOperand(endLabel)));
                instructions.addAll(statementToCode(variableTable, s.getIfStatement(), register));
                instructions.add(new LabInstruction(endLabel));
            }
        }
        else if(statement instanceof FunctionCallStatement){
            FunctionCall fc = ((FunctionCallStatement) statement).getFunctionCall();
            instructions.addAll(expressionToCode(variableTable, fc, register));
//            ret += expressionToCode(variableTable, fc, register);
        } else if (statement instanceof DoWhileStatement) {
            WhileStatement whileStatement = (WhileStatement) statement;
            String loopLabel = newLabel("DOWHILE");
            String endLabel = newLabel("ENDDOWHILE");

            instructions.add(new LabInstruction(loopLabel));


            instructions.addAll(statementToCode(variableTable, whileStatement.getStatement(), register + 1));
            instructions.addAll(expressionToCode(variableTable, whileStatement.getExpression(), register));
            instructions.add(new Instruction("JMPZ", new MemoryOperand("VR", register, true), new MemoryOperand(endLabel)));
            instructions.add(new Instruction("JMP", new MemoryOperand(loopLabel)));
            instructions.add(new LabInstruction(endLabel));
        } else if (statement instanceof WhileStatement) {
            WhileStatement whileStatement = (WhileStatement) statement;
            String loopLabel = newLabel("WHILE");
            String endLabel = newLabel("ENDWHILE");

            instructions.add(new LabInstruction(loopLabel));
            instructions.addAll(expressionToCode(variableTable, whileStatement.getExpression(), register));
            instructions.add(new Instruction("JMPZ", new MemoryOperand("VR", register, true), new MemoryOperand(endLabel)));


            instructions.addAll(statementToCode(variableTable, whileStatement.getStatement(), register + 1));
            instructions.add(new Instruction("JMP", new MemoryOperand(loopLabel)));
            instructions.add(new LabInstruction(endLabel));
        } else if (statement instanceof SwitchStatement) {
            SwitchStatement s = (SwitchStatement) statement;

            String endLabel = newLabel("ENDSWITCH");
            ArrayList<String> caseLabels = new ArrayList<String>();
            ArrayList<Instruction> caseInsts = new ArrayList<Instruction>();
            for (CaseStatement cs : s.getCaseStatements()) {
                String caseLabel = newLabel("CASE");
                instructions.addAll(expressionToCode(variableTable, new BinaryExpression(new BinaryOperator(sym.MINUS),
                        new IntegerValue(new Integer(cs.getN())), new Variable(s.getIdentifier(), sym.INT)), register));
                instructions.add(new Instruction("JMPZ", new MemoryOperand("VR", register, true), new MemoryOperand(caseLabel)));

                caseLabels.add(caseLabel);
                caseInsts.add(new LabInstruction(caseLabel));
                for (Statement st : cs.getStatements()) {
                    caseInsts.addAll(statementToCode(variableTable, st, register));
                }
                if (cs.isBreak())
                    caseInsts.add(new Instruction("JMP", new MemoryOperand(endLabel)));
            }

            CaseStatement cs = s.getDefaultCaseStatement();
            if (cs != null) {
                String caseLabel = newLabel("DEFAULT");
                instructions.add(new Instruction("JMP", new MemoryOperand(caseLabel)));

                caseLabels.add(caseLabel);
                caseInsts.add(new LabInstruction(caseLabel));
                for (Statement st : cs.getStatements()) {
                    caseInsts.addAll(statementToCode(variableTable, st, register));
                }
                if (cs.isBreak())
                    caseInsts.add(new Instruction("JMP", new MemoryOperand(endLabel)));

            } else {
                instructions.add(new Instruction("JMP", new MemoryOperand(endLabel)));
            }
            instructions.addAll(caseInsts);
            instructions.add(new LabInstruction(endLabel));
        } else if (statement instanceof NullStatement) {

        }
        else {
            System.out.println("*"+statement);
        }
        return instructions;
    }

    public String toCode() throws Exception {
        String ret = "";
        for(String area : areaList){
            ret += "    AREA " + area + "\n";
        }
        int stackForVal = 0;
        for (SymbolTableEntry entry : program.getGlobalBlock().getSymbolTable()) {
            if (entry.getRole() == SymbolTableEntry.VARIABLE) {
                Expression index = entry.getVariable().getIndex();
                if (entry.getType().getType() == sym.INT) {

                    if (index == null) {
                        globalVariableTable.put(entry.getVariable().getIdentifier(), new MemoryOperand("MEM", new IntOperand(stackForVal)));
                        stackForVal++;
                    } else {
                        if (!(index instanceof IntegerValue))
                            throw new Exception();
                        ArrayMemoryOperand temp = new ArrayMemoryOperand("MEM", new IntOperand(stackForVal));
                        temp.cnt = ((IntegerValue) index).getValue();
                        globalVariableTable.put(entry.getVariable().getIdentifier(), temp);
                        stackForVal += ((IntegerValue) index).getValue();
                    }
                } else {
                    if (index == null) {
                        globalVariableTable.put(entry.getVariable().getIdentifier(), new FloatMemoryOperand("MEM", new IntOperand(stackForVal)));
                        stackForVal++;
                    } else {
                        if (!(index instanceof IntegerValue))
                            throw new Exception();
                        FloatArrayMemoryOperand temp = new FloatArrayMemoryOperand("MEM", new IntOperand(stackForVal));
                        temp.cnt = ((IntegerValue) index).getValue();
                        globalVariableTable.put(entry.getVariable().getIdentifier(), temp);
                        stackForVal += ((IntegerValue) index).getValue();
                    }

                }
            } else {
                throw new Exception();
            }
        }
        ArrayList<Instruction> instructions = new ArrayList<Instruction>();
        instructions.add(new LabInstruction("START"));
        instructions.add(new Instruction("MOVE", new MemoryOperand("MEM", stackForVal + 2), new MemoryOperand("FP")));
        instructions.add(new Instruction("MOVE", new MemoryOperand("MEM", stackForVal + 2), new MemoryOperand("SP")));
        instructions.add(new Instruction("MOVE", new MemoryOperand("FP@"), new MemoryOperand("SP@", frameInfoSize)));
        instructions.add(new Instruction("MOVE", new LabelOperand("E_main"), new MemoryOperand("FP@", -2)));
        instructions.add(new Instruction("JMP", new LabelOperand("F_main")));
        instructions.add(new LabInstruction("E_main"));
        instructions.add(new LabInstruction("END"));

        for(Function function : program.getFunctionList()){
            instructions.addAll(this.functionToCode(function));
        }

        instructions.add(new LabInstruction("F_printf"));
        instructions.add(new Instruction("WRITE", new MemoryOperand("FP@", -3, true)));
        instructions.add(new Instruction("MOVE", new IntOperand(0), new MemoryOperand("FP@", -1)));
        instructions.add(new Instruction("JMP", new MemoryOperand("FP@", -2, true)));

        instructions.add(new LabInstruction("F_scanf"));
        instructions.add(new Instruction("READ", new MemoryOperand("FP@", -3, true)));
        instructions.add(new Instruction("MOVE", new IntOperand(3), new MemoryOperand("FP@", -1)));
        instructions.add(new Instruction("JMP", new MemoryOperand("FP@", -2, true)));
        ArrayList<Instruction> instructions1 = new ArrayList<Instruction>();
        for (Instruction instruction : instructions) {
            if (!(instruction instanceof Comment) && !instruction.isIgnore) {
                instructions1.add(instruction);
            }
        }
        Instruction frame0 = null;
        Instruction frame1 = null;
        Instruction frame2 = null;
        boolean changed;
        do {
            changed = false;
            ArrayList<Instruction> cur = new ArrayList<Instruction>();
            HashSet<Integer> vrTest = new HashSet<Integer>();
            HashSet<Integer> vrDef = new HashSet<Integer>();
            int len = instructions1.size();
            for (int offset = 0; offset < len - 2; offset++) {

                frame0 = instructions1.get(offset);
                frame1 = instructions1.get(offset + 1);
                frame2 = instructions1.get(offset + 2);
                if (frame0.command != null && frame1.command != null) {

//                    if(frame0.operands != null) {
//                        if(frame0.operands.size() > 1) {
//                            Operand last = frame0.operands.get(frame0.operands.size() - 1);
//                            if (last instanceof MemoryOperand && ((MemoryOperand) last).identifier.equals("VR")
//                                    && vrTest.contains(((IntOperand)((MemoryOperand) last).offset).value)){
//                                vrTest.remove(((IntOperand)((MemoryOperand) last).offset).value);
//                                vrDef.add(((IntOperand)((MemoryOperand) last).offset).value);
//
//                            }
//                        }
//                    }
                    boolean interrupted = false;
//                    for (Operand op : frame0.operands.subList(0, frame0.operands.size() - 1)) {
//                        if (op instanceof MemoryOperand && ((MemoryOperand) op).identifier.equals("VR")
//                                && !vrDef.contains(((IntOperand)((MemoryOperand) op).offset).value)
//                                ) {
//                            if(frame0.operands.get(1) instanceof MemoryOperand &&
//                                    ((MemoryOperand) frame0.operands.get(1)).identifier.equals("SP@")){
////                                cur.add(new Instruction("MOVE", new IntOperand(0), frame0.operands.get(1)));
//                            }
//                            cur.add(new Comment(frame0.toString()));
//                            interrupted = true;
//                            break;
//                        }
//                    }
                    if (interrupted) {
                        changed = true;
                    } else if (frame0.command.equals("JMP") && frame1.command.equals("JMP")) {
                        cur.add(frame0);
                        offset++;
                        changed = true;
                    }
//                    else if (frame0.command.equals("MOVE")
////                            && (frame0.operands.get(0) instanceof IntOperand)
//                            && ((MemoryOperand)(frame0.operands.get(1))).identifier.equals("VR")
//                            && frame1.command.equals("MOVE")
//                            && frame1.operands.get(0) instanceof MemoryOperand
//                            && ((MemoryOperand)(frame1.operands.get(0))).identifier.equals("VR")
//                            && ((MemoryOperand)(frame1.operands.get(0))).offset.equals(((MemoryOperand) (frame0.operands.get(1))).offset)
//                            ) {
//                        ArrayList<Operand> newOps = new ArrayList<Operand>();
//                        newOps.add(frame0.operands.get(0));
//                        newOps.add(frame1.operands.get(1));
//                        cur.add(new Comment(frame0.toString()));
//                        frame1.operands = newOps;
//                        int val = ((IntOperand)((MemoryOperand) (frame0.operands.get(1))).offset).getValue();
//                        vrTest.add(val);
//                        if(vrDef.contains(new Integer(val)))
//                            vrDef.remove(new Integer(val));
////                        cur.add(new Comment(vrTest.toString()));
////                        cur.add(new Comment(vrDef.toString()));
//                        changed = true;
//                    }
                    else {
                        cur.add(frame0);
                    }

                } else {
                    cur.add(frame0);
                }
            }
            cur.add(frame1);
            cur.add(frame2);
            instructions1 = cur;
        } while (changed);
        for (Instruction instruction : instructions1) {
            ret += instruction + "\n";
        }
        return ret + "\n";
    }
}
