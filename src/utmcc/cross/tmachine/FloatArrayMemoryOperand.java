/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc.cross.tmachine;

/**
 * Created by uptown on 2014. 6. 12..
 */
public class FloatArrayMemoryOperand extends FloatMemoryOperand {
    public int cnt;

    public FloatArrayMemoryOperand(String identifier, Operand offset) {
        super(identifier, offset);
    }

    public FloatArrayMemoryOperand(String identifier, Operand offset, boolean isValue) {
        super(identifier, offset, isValue);
    }

    public FloatArrayMemoryOperand(MemoryOperand operand, boolean isValue) {
        super(operand, isValue);
    }

    public FloatArrayMemoryOperand(String identifier, int offset) {
        super(identifier, offset);
    }

    public FloatArrayMemoryOperand(String identifier, int offset, boolean isValue) {
        super(identifier, offset, isValue);
    }

    public FloatArrayMemoryOperand(String identifier) {
        super(identifier);
    }

    public FloatArrayMemoryOperand(String identifier, boolean isValue) {
        super(identifier, isValue);
    }
}
