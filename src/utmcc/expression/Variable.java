/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc.expression;

import utmcc.sym;

/**
 * Created by uptown on 2014. 4. 12..
 */
public class Variable extends Expression {
    String identifier;
    Expression index;
    int type;
    int casted;

    public Variable(String identifier, int type) {
        this.identifier = identifier;
        this.type = type;
        index = null;
        casted = 0;
    }

    public Variable(String identifier, Expression index, int type) {
        this.identifier = identifier;
        this.type = type;
        this.index = index;
    }

    public String toString() {
        String ret = "";
        if (casted != 0) {
            if (casted == sym.INT)
                ret += "(int)";
            else
                ret += "(float)";
        }
        if (this.index != null) {
            ret += this.identifier + '[' + this.index + ']';
            return ret;
        }
        ret += this.identifier;
        return ret;
    }

    public String getIdentifier() {
        return identifier;
    }

    public Expression getIndex() {
        return index;
    }

    @Override
    public int expectedType() {
        if (casted != 0) {
            return casted;
        }
        return this.type;
    }

    public Expression typeCast(int type) {
        if (type != this.type) {
            casted = type;
        }
        return this;
    }

    public int getType() {
        return type;
    }

    public int getCasted() {
        return casted;
    }
//    @Override
//    public int hashCode() {
//        return identifier.hashCode();
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if(obj instanceof Variable){
//            return ((Variable) obj).getIdentifier().equals(this.getIdentifier());
//        }
//        return super.equals(obj);
//    }
}
