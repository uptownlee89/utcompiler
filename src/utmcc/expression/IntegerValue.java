/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc.expression;

import utmcc.sym;

/**
 * Created by uptown on 2014. 4. 12..
 */
public class IntegerValue extends Value {

    int value;

    public IntegerValue(Integer intVal) {
        this.type = sym.INT;
        this.value = intVal.intValue();
    }

    public Number asNumber() {
        return new Integer(this.value);
    }

    public int getValue() {
        return value;
    }

    public Value divide(Value right) {
        int retType = right.type == sym.FLOAT ? sym.FLOAT : sym.INT;
        if (retType == sym.FLOAT) {
            return new FloatValue(this.value / ((FloatValue) right).value);
        } else {
            return new IntegerValue(this.value / ((IntegerValue) right).value);
        }
    }

    public Value plus(Value right) {
        int retType = right.type == sym.FLOAT ? sym.FLOAT : sym.INT;
        if (retType == sym.FLOAT) {
            return new FloatValue(this.value + ((FloatValue) right).value);
        } else {
            return new IntegerValue(this.value + ((IntegerValue) right).value);
        }
    }

    public Value minus(Value right) {
        int retType = right.type == sym.FLOAT ? sym.FLOAT : sym.INT;
        if (retType == sym.FLOAT) {
            return new FloatValue(this.value - ((FloatValue) right).value);
        } else {
            return new IntegerValue(this.value - ((IntegerValue) right).value);
        }
    }

    public Value times(Value right) {
        int retType = right.type == sym.FLOAT ? sym.FLOAT : sym.INT;
        if (retType == sym.FLOAT) {
            return new FloatValue(this.value * ((FloatValue) right).value);
        } else {
            return new IntegerValue(this.value * ((IntegerValue) right).value);
        }
    }

    public IntegerValue or(Value right) {
        int retType = right.type == sym.FLOAT ? sym.FLOAT : sym.INT;
        if (retType == sym.FLOAT) {
            return new IntegerValue((this.value != 0 || ((FloatValue) right).value != 0) ? 1 : 0);
        } else {
            return new IntegerValue((this.value != 0 || ((IntegerValue) right).value != 0) ? 1 : 0);
        }
    }

    public IntegerValue and(Value right) {
        int retType = right.type == sym.FLOAT ? sym.FLOAT : sym.INT;
        if (retType == sym.FLOAT) {
            return new IntegerValue((this.value != 0 && ((FloatValue) right).value != 0) ? 1 : 0);
        } else {
            return new IntegerValue((this.value != 0 && ((IntegerValue) right).value != 0) ? 1 : 0);
        }
    }

    public IntegerValue equals(Value right) {
        int retType = right.type == sym.FLOAT ? sym.FLOAT : sym.INT;
        if (retType == sym.FLOAT) {
            return this.value == ((FloatValue) right).value ? new IntegerValue(1) : new IntegerValue(0);
        } else {
            return this.value == ((IntegerValue) right).value ? new IntegerValue(1) : new IntegerValue(0);
        }
    }

    public IntegerValue greaterThan(Value right) {
        int retType = right.type == sym.FLOAT ? sym.FLOAT : sym.INT;
        if (retType == sym.FLOAT) {
            return this.value < ((FloatValue) right).value ? new IntegerValue(1) : new IntegerValue(0);
        } else {
            return this.value < ((IntegerValue) right).value ? new IntegerValue(1) : new IntegerValue(0);
        }
    }

    public IntegerValue not() {
        return new IntegerValue(this.value != 0 ? 0 : 1);
    }

    public String toString() {
        return (new Integer(this.value)).toString();
    }

    @Override
    public int expectedType(){
        return sym.INT;
    }

    public Expression typeCast(int type){
        if(type == sym.FLOAT){
            return new FloatValue(new Float((float)this.value));
        }
        return this;
    }
}
