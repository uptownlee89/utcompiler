/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc.expression;

import utmcc.sym;
import utmcc.token.BinaryOperator;

/**
 * Created by uptown on 2014. 4. 13..
 */
public class BinaryExpression extends Expression {
    BinaryOperator operator;
    Expression e0;

    public BinaryOperator getOperator() {
        return operator;
    }

    public Expression getE0() {
        return e0;
    }

    public Expression getE1() {
        return e1;
    }

    Expression e1;

    public BinaryExpression(BinaryOperator operator, Expression e0, Expression e1) {
        this.operator = operator;
        this.e0 = e0;
        this.e1 = e1;
    }

    public String toString() {
        return String.format("%s %s %s", e0, operator, e1);
    }

    @Override
    public int expectedType(){
        if(operator.getType() == sym.GT || operator.getType() == sym.LT
                || operator.getType() == sym.GTE
                || operator.getType() == sym.LTE
                || operator.getType() == sym.EQUALS
                || operator.getType() == sym.DIFF
                )
            return sym.INT;
        int type0 = e0.expectedType();
        int type1 = e1.expectedType();
        if(type0 != type1){
            return sym.FLOAT;
        }
        return type0;
    }

    public Expression typeCast(int type){
        if(type != this.expectedType()){
            return new BracketExpression(this, type);
        }
        return this;
    }

    public void setE0(Expression e0) {
        this.e0 = e0;
    }

    public void setE1(Expression e1) {
        this.e1 = e1;
    }
}
