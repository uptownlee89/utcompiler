/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc.expression;

import utmcc.Function;
import utmcc.Program;
import utmcc.sym;

import java.util.ArrayList;

/**
 * Created by uptown on 2014. 4. 13..
 */
public class FunctionCall extends Expression {
    public int type;
    String identifier;
    ArrayList<Expression> arguments;
    int casted;

    public FunctionCall(String identifier, ArrayList<Expression> arguments) {
        this.identifier = identifier;
        this.arguments = arguments;
        casted = 0;
    }

    @Override
    public int expectedType() {
        Function function = Program.gp.getFunction(identifier);
        if (function != null) {
            type = function.getReturnType().getType();
        }
        if (casted != 0) {
            return casted;
        }
        return this.type;
    }

    public String getIdentifier() {
        return identifier;
    }

    public ArrayList<Expression> getArguments() {
        return arguments;
    }

    @Override
    public String toString() {
        String ret = "";
        if (casted != 0) {
            if (casted == sym.INT)
                ret += "(int)";
            else
                ret += "(float)";
        }
        ret += this.identifier + "(";
        if (arguments != null) {
            int size = arguments.size();
            for (Expression e : arguments) {

                if (--size == 0) {
                    ret += e;
                } else {
                    ret += e + ", ";

                }
            }
        }
        return ret + ")";
    }

    public Expression typeCast(int type) {
        if (type != this.type)
            casted = type;
        return this;
    }

    public int getCasted() {
        return casted;
    }

    public int getType() {
        return type;
    }
}
