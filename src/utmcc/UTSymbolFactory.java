/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc;

import java_cup.runtime.ComplexSymbolFactory;
import java_cup.runtime.Symbol;

/**
 * Created by uptown on 2014. 4. 14..
 */
public class UTSymbolFactory extends ComplexSymbolFactory {

    protected Compiler compiler = null;

    public Symbol newSymbol(String name, int id, Location left, Location right, Object value) {
        return new SmartSymbol(name, id, left, right, value, compiler.parser.getLastSymbol());
    }

    public Symbol newSymbol(String name, int id, Location left, Location right) {
        return new SmartSymbol(name, id, left, right, compiler.parser.getLastSymbol());
    }

    public Symbol newSymbol(String name, int id, Symbol left, Symbol right, Object value) {
        return new SmartSymbol(name, id, left, right, value);
    }

    public Symbol newSymbol(String name, int id, Symbol left, Symbol right) {
        return new SmartSymbol(name, id, left, right);
    }

    public Symbol newSymbol(String name, int id) {
        return new SmartSymbol(name, id, compiler.parser.getLastSymbol());
    }

    public Symbol startSymbol(String name, int id, int state) {
        return new SmartSymbol(name, id, state, compiler.parser.getLastSymbol());
    }

    public class SmartSymbol extends ComplexSymbol {

        Symbol leftSymbol;

        public SmartSymbol(String name, int id, Symbol leftSymbol) {
            super(name, id);
            this.leftSymbol = leftSymbol;
        }

        public SmartSymbol(String name, int id, int state, Symbol leftSymbol) {
            super(name, id, state);
            this.leftSymbol = leftSymbol;
        }

        public SmartSymbol(String name, int id, Symbol left, Symbol right) {
            super(name, id, left, right);
            this.leftSymbol = left;
        }

        public SmartSymbol(String name, int id, Location left, Location right, Symbol leftSymbol) {
            super(name, id, left, right);
            this.leftSymbol = leftSymbol;
        }

        public SmartSymbol(String name, int id, Symbol left, Symbol right, Object value) {
            super(name, id, left, right, value);
            this.leftSymbol = left;
        }

        public SmartSymbol(String name, int id, Location left, Location right, Object value, Symbol leftSymbol) {
            super(name, id, left, right, value);
            this.leftSymbol = leftSymbol;
        }

        public Symbol getLeftSymbol() {
            return leftSymbol;
        }

    }
}
