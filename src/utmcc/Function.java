/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc;

import utmcc.statement.CompoundStatement;
import utmcc.token.Type;

import java.util.ArrayList;

/**
 * Created by uptown on 2014. 4. 29..
 */
public class Function {
    public DebugInfo debugInfo;

    public String getIdentifier() {
        return identifier;
    }

    public Type getReturnType() {
        return returnType;
    }

    public ArrayList<Parameter> getParameters() {
        return parameters;
    }

    public CompoundStatement getStatement() {
        return statement;
    }

    String identifier;
    Type returnType;
    ArrayList<Parameter> parameters;
    CompoundStatement statement;



    public Function(Type returnType, String identifier, ArrayList<Parameter> parameters, CompoundStatement statement){
        this.identifier = identifier;
        this.returnType = returnType;
        this.parameters = parameters;
        this.statement = statement;
    }

    @Override
    public String toString(){
        String ret = returnType.toString();
        ret += " " + identifier + "(";
        int size = parameters.size();
        for(Parameter p : parameters){
            ret += p;
            if(--size != 0){
                ret += ", ";
            }
        }
        ret += ")\n";
        ret += statement.toString();
        return ret;
    }
}
