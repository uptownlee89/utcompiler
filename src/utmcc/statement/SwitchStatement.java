/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc.statement;

import utmcc.Block;

import java.util.ArrayList;

/**
 * Created by uptown on 2014. 4. 30..
 */
public class SwitchStatement extends Statement{
    String identifier;
    CaseStatement defaultCaseStatement;
    ArrayList<CaseStatement> caseStatements;

    public String getIdentifier() {
        return identifier;
    }

    public CaseStatement getDefaultCaseStatement() {
        return defaultCaseStatement;
    }

    public ArrayList<CaseStatement> getCaseStatements() {
        return caseStatements;
    }

    public SwitchStatement(Block currentBlock, String identifier, ArrayList<CaseStatement> caseStatements, CaseStatement defaultCaseStatement){
        super(currentBlock);
        this.identifier = identifier;
        this.caseStatements = caseStatements;
        this.defaultCaseStatement = defaultCaseStatement;
    }

    public void setIdentifier(String identifier){ this.identifier = identifier; }


    @Override
    public String toString(){
        String prefix = this.tabPrefix();
        String ret = prefix.replaceFirst("  ", "") + "switch(" + identifier + "){\n";
        if(caseStatements != null) {
            for (Statement s : caseStatements){
                ret += s;
            }
        }
        if(defaultCaseStatement != null){
            ret += defaultCaseStatement;
        }
        ret += prefix.replaceFirst("  ", "") + "}";
        return ret;
    }
}
