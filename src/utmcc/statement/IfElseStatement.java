/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc.statement;

import utmcc.Block;
import utmcc.expression.Expression;


/**
 * Created by uptown on 2014. 4. 30..
 */
public class IfElseStatement extends Statement {
    Statement ifStatement;
    Statement elseStatement;
    Expression expression;

    public IfElseStatement(Block currentBlock, Expression expression, Statement ifStatement, Statement elseStatement){
        super(currentBlock);
        this.expression = expression;
        this.ifStatement = ifStatement;
        this.elseStatement = elseStatement;
    }

    @Override
    public String toString() {
        String prefix = this.tabPrefix();
        String ret = prefix + "if(" + expression + ")\n";
        if(ifStatement instanceof CompoundStatement) {
            ret += ifStatement;
        }
        else{
            ret += "  " + ifStatement;
        }
        if(elseStatement != null){
            ret += "\n" + prefix + "else \n";
            if(elseStatement instanceof CompoundStatement) {
                ret += elseStatement;
            }
            else{
                ret += "  " + elseStatement;
            }
        }
        return ret;
    }

    public Statement getIfStatement() {
        return ifStatement;
    }

    public Statement getElseStatement() {
        return elseStatement;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }
}
