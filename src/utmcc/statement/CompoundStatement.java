/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc.statement;

import utmcc.Block;
import utmcc.Declaration;

import java.util.ArrayList;

/**
 * Created by uptown on 2014. 4. 29..
 */
public class CompoundStatement extends Statement {
    public ArrayList<Declaration> getDeclarations() {
        return declarations;
    }

    public ArrayList<Statement> getStatements() {
        return statements;
    }

    ArrayList<Declaration> declarations;
    ArrayList<Statement> statements;

    public CompoundStatement(Block currentBlock, ArrayList<Declaration> declarations, ArrayList<Statement> statements){
        super(currentBlock);
        this.declarations = declarations;
        this.statements = statements;
    }

    public void setStatements(ArrayList<Statement> statements) {
        this.statements = statements;
    }

    @Override
    public String toString(){
        String prefix = this.tabPrefix();
        String ret = prefix.replaceFirst("  ", "") + "{\n";
        for(Declaration dec : declarations){
            ret += prefix + dec + "\n";
        }
        for(Statement statement : statements){
            ret += statement + "\n";
        }
        ret += prefix.replaceFirst("  ", "") + "}";
        return ret;
    }

    @Override
    public void setBlock(Block currentBlock){

        for(Statement statement : statements){
            statement.setBlock(currentBlock);
        }
        this.block = currentBlock;
    }
}
