/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc.statement;

import utmcc.Block;
import utmcc.expression.Expression;

/**
 * Created by uptown on 2014. 4. 30..
 */
public class ForStatement extends Statement {
    Assign initAssign;
    Assign loopAssign;
    Expression expression;
    Statement statement;

    public Assign getInitAssign() {
        return initAssign;
    }

    public Assign getLoopAssign() {
        return loopAssign;
    }

    public Expression getExpression() {
        return expression;
    }

    public Statement getStatement() {
        return statement;
    }

    public ForStatement(Block currentBlock, Assign initAssign, Assign loopAssign, Expression expression, Statement statement){
        super(currentBlock);
        this.initAssign = initAssign;
        this.loopAssign = loopAssign;
        this.expression = expression;
        this.statement = statement;
    }

    @Override
    public String toString() {
        String prefix = this.tabPrefix();
        String ret = prefix + "for(" + initAssign + "; " + expression + "; " + loopAssign + ")\n";
        ret += statement;
        return ret;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }
}
