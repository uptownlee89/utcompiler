/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc.statement;


import utmcc.Block;

import java.util.ArrayList;

/**
 * Created by uptown on 2014. 4. 30..
 */
public class DefaultCaseStatement extends Statement {
    ArrayList<Statement> statements;
    boolean isBreak;

    public ArrayList<Statement> getStatements() {
        return statements;
    }

    public DefaultCaseStatement(Block currentBlock, ArrayList<Statement> statements, boolean isBreak){
        super(currentBlock);
        this.statements = statements;
        this.isBreak = isBreak;
    }

    @Override
    public String toString() {
        String prefix = this.tabPrefix();
        String ret = "";
        ret += prefix + "default:\n";
        for(Statement statement : statements){
            ret += statement + "\n";
        }
        if(isBreak){
            ret += prefix + "break;\n";
        }
        return ret;
    }
}
