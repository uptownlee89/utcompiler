/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc.statement;

import utmcc.expression.Expression;

/**
 * Created by uptown on 2014. 4. 30..
 */
public class Assign {
    String identifier;
    Expression index;
    Expression expression;

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    public Assign(String identifier, Expression expression){
        this.identifier = identifier;
        this.index = null;

        this.expression = expression;
    }
    public Assign(String identifier, Expression index, Expression expression){
        this.identifier = identifier;
        this.index = index;
        this.expression = expression;
    }

    @Override
    public String toString() {
        if(index != null){
            return identifier + "[" + index + "] = " + expression;
        }
        return identifier + " = " + expression;
    }

    public String getIdentifier() {
        return identifier;
    }

    public Expression getIndex() {
        return index;
    }

    public Expression getExpression() {
        return expression;
    }
}
