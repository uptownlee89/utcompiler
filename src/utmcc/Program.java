/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc;

import java.util.ArrayList;

/**
 * Created by uptown on 2014. 4. 29..
 */
public class Program {

    public static Program gp = null;
    public ArrayList<String> lookup = new ArrayList<String>();
    ArrayList<utmcc.error.Error> errors;
    private Block currentBlock;
    private Block globalBlock;
    private ArrayList<Declaration> declarationList;
    private ArrayList<Function> functionList;

    public Program() {
        currentBlock = new Block("GLOBAL", null, Block.GLOBAL); // Global Block
        globalBlock = currentBlock;
        declarationList = new ArrayList<Declaration>();
        functionList = new ArrayList<Function>();
        errors = new ArrayList<utmcc.error.Error>();
        gp = this;
    }

    public ArrayList<Declaration> getDeclarationList() {
        return declarationList;
    }

    protected void setDeclarationList(ArrayList<Declaration> declarationList) {
        this.declarationList = declarationList;
    }

    public ArrayList<Function> getFunctionList() {
        return functionList;
    }

    protected void setFunctionList(ArrayList<Function> functionList) {
        this.functionList = functionList;
    }

    public ArrayList<utmcc.error.Error> errors() {
        return errors;
    }

    protected void addError(utmcc.error.Error e) {
        errors.add(e);
    }

    protected void pushBlock(String name) {
        currentBlock = currentBlock.createChild(name, Block.COMPOUND);
    }

    protected void pushFunctionBlock(String name) {
        currentBlock = currentBlock.createChild(name, Block.FUNCTION);
    }

    public Block getGlobalBlock() {
        return globalBlock;
    }

    public Block getFunctonBlock(String name) {
        return globalBlock.findChild(Block.FUNCTION, name);
    }

    protected void popBlock() {
        currentBlock = currentBlock.getParent();
    }

    public Function getFunction(String name) {
        for (Function function : functionList) {
            if (function.getIdentifier().equals(name)) {
                return function;
            }
        }
        return null;
    }

    protected Block getCurrentBlock() {
        return currentBlock;
    }

    @Override
    public String toString() {
        String ret = "";
        for (Declaration dec : declarationList) {
            ret += dec + "\n";
        }
        for (Function func : functionList) {
            ret += func + "\n";
        }
        return ret;
    }

}
