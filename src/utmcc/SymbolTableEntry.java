/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc;

import utmcc.expression.Variable;
import utmcc.token.Type;

/**
 * Created by uptown on 2014. 4. 12..
 */
public class SymbolTableEntry {
    protected Type type;
    protected Variable variable;
    protected int role;
    protected boolean isInit;
    public static int PARAMETER = 0, VARIABLE = 1, FUNCTION = 2;

    public int getRole() {
        return role;
    }

    public Type getType() {
        return type;
    }

    public void setInit(boolean isInit){this.isInit = isInit;}

    public boolean isInit(){return isInit;}

    public Variable getVariable() {
        return variable;
    }

    public SymbolTableEntry(Type type, Variable variable, int role) {
        this.variable = variable;
        this.type = type;
        this.role = role;
    }

    @Override
    public int hashCode() {
        return variable.getIdentifier().hashCode();
    }
}

/*
Symbol table entry := <symbol name, symbol info>
Symbol info := <type, location, pointer to its associated declaration>
location := level of nested blocks and order of blocks

* */