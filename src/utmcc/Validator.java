/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc;

import utmcc.error.ScopeError;
import utmcc.error.TypeError;
import utmcc.expression.*;
import utmcc.statement.*;
import utmcc.token.Type;

import java.util.ArrayList;

/**
 * Created by uptown on 2014. 5. 18..
 */
public class Validator {
    Program program;
    boolean isScanf;

    public Validator(Program program) {
        this.program = program;
        this.isScanf = false;
    }

    public void validate() throws Exception {
        for (Function function : program.getFunctionList()) {
            this.validateFunction(function);
        }
    }

    public void validateExpression(Expression expression, Statement statement) throws Exception {
        validateExpression(expression, statement, 0);
    }

    public void validateExpression(Expression expression, Statement statement, int flag) throws Exception {
        if (expression instanceof FunctionCall) {
            FunctionCall call = (FunctionCall) expression;
            String identifier = ((FunctionCall) expression).getIdentifier();
            Function function = program.getFunction(identifier);
            boolean isBuiltin = false;
            if (function != null) {
                ((FunctionCall) expression).type = function.getReturnType().getType();
            } else if (identifier.equals("printf")) {
                ((FunctionCall) expression).type = sym.INT;
                isBuiltin = true;
            } else if (identifier.equals("scanf")) {
                ((FunctionCall) expression).type = sym.INT;
                isBuiltin = true;
            } else {
                throw new ScopeError("'" + call.getIdentifier() + "' is not function.", expression.debugInfo.start, expression.debugInfo.end, null);
            }
            call.getArguments();
            ArrayList<Parameter> parameters;
            if (isBuiltin) {
                parameters = new ArrayList<Parameter>();
                parameters.add(new Parameter(new Type(sym.INT), new Variable("val", sym.INT)));
            } else {
                parameters = function.getParameters();
            }
            ArrayList<Expression> args = call.getArguments();
            if (args == null) {
                args = new ArrayList<Expression>();
            }
            if (parameters.size() != args.size()) {
                throw new TypeError("'" + call.getIdentifier() + "' needs " + parameters.size() + " parameters, but parameters' count is " + args.size() + ".", expression.debugInfo.start, expression.debugInfo.end, null);
            }
            if (parameters.size() > 0) {
                int i = 0;
                Parameter cur = parameters.get(i);
                while (true) {
                    Expression arg = args.get(i);
                    if (cur.identifier.getIndex() != null) {
                        if (cur.type.getType() != arg.expectedType()) {
                            String aa = cur.type.getType() == sym.INT ? "int" : "float";
                            throw new TypeError("'" + call.getIdentifier() + "''s " + (i + 1) + " parameter cannot be converted into " + aa + " because it is array parameter.", expression.debugInfo.start, expression.debugInfo.end, null);
                        } else if (arg instanceof Variable && statement.getBlock().getVariable(((Variable) arg).getIdentifier()).variable.getIndex() != null &&
                                ((Variable) arg).getIndex() == null) {

                        } else {
                            throw new TypeError("'" + call.getIdentifier() + "''s " + (i + 1) + " parameter cannot be converted into array parameter.", expression.debugInfo.start, expression.debugInfo.end, null);
                        }

                    } else {
                        if (cur.type.getType() != arg.expectedType()) {
                            String aa = cur.type.getType() == sym.INT ? "int" : "float";
                            System.out.println("Warning: '" + call.getIdentifier() + "''s " + (i + 1) + " parameter is expected as " + aa + ". " + call.debugInfo);
                            args.set(i, arg.typeCast(cur.type.getType()));
                        }
                    }
                    validateExpression(arg, statement, 1);
                    try {
                        cur = parameters.get(++i);
                    } catch (IndexOutOfBoundsException e) {
                        break;
                    }

                }
            }
        } else if (expression instanceof BracketExpression) {
            BracketExpression bracketExpression = (BracketExpression) expression;
            validateExpression(bracketExpression.getExpression(), statement);
        } else if (expression instanceof Variable) {
            Variable variable = (Variable) expression;
            if (variable.getIndex() != null && variable.getIndex().expectedType() != sym.INT) {
                throw new TypeError("'" + variable.getIdentifier() + "''s index must be int type.", expression.debugInfo.start, expression.debugInfo.end, null);
            }

            if (variable.getIndex() != null) {
                if (variable.getIndex().expectedType() != sym.INT) {
                    throw new TypeError("'" + variable.getIdentifier() + "''s index must be int type.", expression.debugInfo.start, expression.debugInfo.end, null);
                }
                if (statement.getBlock().getVariable(variable.getIdentifier()).getVariable().getIndex() == null) {
                    throw new TypeError("'" + variable.getIdentifier() + "' is not array.", expression.debugInfo.start, expression.debugInfo.end, null);
                }
            } else {
                if (flag != 1) {
                    if (statement.getBlock().getVariable(variable.getIdentifier()).getVariable().getIndex() != null) {
                        throw new TypeError("'" + variable.getIdentifier() + "' is array.", expression.debugInfo.start, expression.debugInfo.end, null);
                    }
                }
                SymbolTableEntry entry = statement.getBlock().getVariable(variable.getIdentifier());
                if (entry.variable.getIndex() == null) {
                    //if (!entry.isInit() && !isScanf) {
                    //    throw new TypeError("'" + variable.getIdentifier() + "' is not init.", expression.debugInfo.start, expression.debugInfo.end, null);
                    //}
                }
            }
        } else if (expression instanceof BinaryExpression) {
            BinaryExpression binaryExpression = (BinaryExpression) expression;
            validateExpression(binaryExpression.getE0(), statement);
            validateExpression(binaryExpression.getE1(), statement);
            int type0 = binaryExpression.getE0().expectedType();
            int type1 = binaryExpression.getE1().expectedType();
            switch (((BinaryExpression) expression).getOperator().getType()) {
                case sym.PLUS:
                case sym.MINUS:
                case sym.TIMES:
                case sym.DIV:
                case sym.GT:
                case sym.GTE:
                case sym.LT:
                case sym.LTE:
                case sym.DIFF:
                case sym.EQUALS:
                    if (type0 != type1) {
                        System.out.println("Warning: '" + binaryExpression.getE0() + "' and '" + binaryExpression.getE1() + "''s expected type are not same . " + binaryExpression.debugInfo);
                        if (type0 == sym.INT) {
                            binaryExpression.setE0(binaryExpression.getE0().typeCast(sym.FLOAT));
                        } else {
                            binaryExpression.setE1(binaryExpression.getE1().typeCast(sym.FLOAT));
                        }
                    }
                    break;
//                    if(type0 != type1){
//                    }
//                    break;
            }
        } else if (expression instanceof UnaryExpression) {
            UnaryExpression unaryExpression = (UnaryExpression) expression;
            validateExpression(unaryExpression.getE(), statement);
        }
    }

    public void validateAssign(Assign assign, Statement statement0) throws Exception {

        if (assign.getExpression() instanceof FunctionCall) {
            String identifier = ((FunctionCall) assign.getExpression()).getIdentifier();
            if (identifier.equals("printf")) {
                ((FunctionCall) assign.getExpression()).type = sym.INT;
            } else if (identifier.equals("scanf")) {
                ((FunctionCall) assign.getExpression()).type = sym.INT;
            } else {
                ((FunctionCall) assign.getExpression()).type = program.getFunction(((FunctionCall) assign.getExpression()).getIdentifier()).getReturnType().getType();
            }
        }
        int type0 = statement0.getBlock().getVariable(assign.getIdentifier()).getType().getType();
        int type1 = assign.getExpression().expectedType();
        if (type0 == sym.INT && type1 == sym.FLOAT) {
            System.out.println("Warning: '" + assign.getIdentifier() + "' is int type and '" + assign.getExpression() + "' is float type. " + statement0.debugInfo);
            assign.setExpression(assign.getExpression().typeCast(sym.INT));
        }
        if (type0 == sym.FLOAT && type1 == sym.INT) {
            System.out.println("Warning: '" + assign.getIdentifier() + "' is float type and '" + assign.getExpression() + "' is int type. " + statement0.debugInfo);
            assign.setExpression(assign.getExpression().typeCast(sym.FLOAT));
        }
        if (assign.getIndex() != null) {
            if (assign.getIndex().expectedType() != sym.INT) {
                throw new TypeError("'" + assign.getIdentifier() + "''s index must be int type.", statement0.debugInfo.start, statement0.debugInfo.end, null);
            }
            if (statement0.getBlock().getVariable(assign.getIdentifier()).getVariable().getIndex() == null) {
                throw new TypeError("'" + assign.getIdentifier() + "' is not array.", statement0.debugInfo.start, statement0.debugInfo.end, null);
            }
        } else {
            if (statement0.getBlock().getVariable(assign.getIdentifier()).getVariable().getIndex() != null) {
                throw new TypeError("'" + assign.getIdentifier() + "' is array.", statement0.debugInfo.start, statement0.debugInfo.end, null);
            }
        }
        validateExpression(assign.getExpression(), statement0);

    }

    public void validateStatement(Statement statement, Function function) throws Exception {
        if (statement instanceof CompoundStatement) {

            CompoundStatement statement0 = (CompoundStatement) statement;
            boolean hasRet = false;
            for (Statement each : statement0.getStatements()) {
                if (hasRet) {
                    System.out.println("Warning: other statements after return statement. " + each.debugInfo);
                }
                if (statement instanceof ReturnStatement) {
                    hasRet = true;
                }
                validateStatement(each, function);
            }
        } else if (statement instanceof ReturnStatement) {
            ReturnStatement ret = (ReturnStatement) statement;
            Expression expression = ret.getExpression();
            int type;
            if (expression != null) {
                type = ret.getExpression().expectedType();
            } else {
                type = function.getReturnType().getType();
            }
            if (function.getReturnType().getType() == sym.INT && type == sym.FLOAT) {
                System.out.println("Warning: " + function.getIdentifier() + " must have int return statement. " + statement.debugInfo);
                ret.setExpression(ret.getExpression().typeCast(sym.INT));
            } else if (function.getReturnType().getType() == sym.FLOAT && type == sym.INT) {
                System.out.println("Warning: " + function.getIdentifier() + " must have float return statement. " + statement.debugInfo);
                ret.setExpression(ret.getExpression().typeCast(sym.FLOAT));
            }
        } else if (statement instanceof SwitchStatement) {
            SwitchStatement statement0 = (SwitchStatement) statement;
            validateStatement(statement0.getDefaultCaseStatement(), function);
            for (Statement each : statement0.getCaseStatements()) {
                validateStatement(each, function);
            }
            SymbolTableEntry variable = statement0.getBlock().getVariable(statement0.getIdentifier());
            if (variable.getType().getType() == sym.FLOAT) {
                throw new TypeError("Switch: '" + variable.getVariable().getIdentifier() + "' is not int type.", statement0.debugInfo.start, statement0.debugInfo.end, null);
            }

        } else if (statement instanceof AssignStatement) {
            AssignStatement statement0 = (AssignStatement) statement;
            Assign assign = statement0.getAssign();
            validateAssign(assign, statement0);
        } else if (statement instanceof IfElseStatement) {
            IfElseStatement ifElseStatement = (IfElseStatement) statement;
            if (ifElseStatement.getExpression().expectedType() != sym.INT) {
                System.out.println("Warning: if statement's expression must be int type. " + ifElseStatement.debugInfo);
                ifElseStatement.setExpression(ifElseStatement.getExpression().typeCast(sym.INT));
            }
            validateExpression(ifElseStatement.getExpression(), ifElseStatement);
            validateStatement(ifElseStatement.getIfStatement(), function);
            if (ifElseStatement.getElseStatement() != null)
                validateStatement(ifElseStatement.getElseStatement(), function);
        } else if (statement instanceof WhileStatement) {
            WhileStatement ifElseStatement = (WhileStatement) statement;
            if (ifElseStatement.getExpression().expectedType() != sym.INT) {
                System.out.println("Warning: while statement's expression must be int type. " + ifElseStatement.debugInfo);
                ifElseStatement.setExpression(ifElseStatement.getExpression().typeCast(sym.INT));
            }
            validateStatement(ifElseStatement.getStatement(), function);
        } else if (statement instanceof DoWhileStatement) {
            DoWhileStatement ifElseStatement = (DoWhileStatement) statement;
            if (ifElseStatement.getExpression().expectedType() != sym.INT) {
                System.out.println("Warning: while statement's expression must be int type. " + ifElseStatement.debugInfo);
                ifElseStatement.setExpression(ifElseStatement.getExpression().typeCast(sym.INT));
            }
            validateStatement(ifElseStatement.getStatement(), function);
        } else if (statement instanceof CaseStatement) {
            CaseStatement caseStatement = (CaseStatement) statement;
            for (Statement each : caseStatement.getStatements()) {
                validateStatement(each, function);
            }
        } else if (statement instanceof DefaultCaseStatement) {
            DefaultCaseStatement caseStatement = (DefaultCaseStatement) statement;
            for (Statement each : caseStatement.getStatements()) {
                validateStatement(each, function);
            }
        } else if (statement instanceof ForStatement) {
            ForStatement forStatement = (ForStatement) statement;
            validateAssign(forStatement.getInitAssign(), statement);
            validateAssign(forStatement.getLoopAssign(), statement);
            validateExpression(forStatement.getExpression(), statement);
            if (forStatement.getExpression().expectedType() != sym.INT) {
                System.out.println("Warning: for statement's expression must be int type. " + forStatement.debugInfo);
                forStatement.setExpression(forStatement.getExpression().typeCast(sym.INT));
            }
            validateStatement(forStatement.getStatement(), function);
        } else if (statement instanceof FunctionCallStatement) {
            FunctionCallStatement functionCallStatement = (FunctionCallStatement) statement;
            if (functionCallStatement.getFunctionCall().getIdentifier().equals("scanf"))
                isScanf = true;
            else
                isScanf = false;
            validateExpression(functionCallStatement.getFunctionCall(), statement);
        }
    }

    public void validateFunction(Function function) throws Exception {
        boolean hasRet = false;
        ;
        for (Statement statement : function.getStatement().getStatements()) {
            if (hasRet) {
                System.out.println("Warning: " + function.getIdentifier() + " have other statements after return statement. " + function.debugInfo);
            }
            if (statement instanceof ReturnStatement) {
                hasRet = true;
            }
            validateStatement(statement, function);
        }
        if (!hasRet) {
            System.out.println("Warning: " + function.getIdentifier() + " has no return statement. " + function.debugInfo);
            ArrayList<Statement> statementArrayList = function.getStatement().getStatements();
            if (function.getReturnType().getType() == sym.INT)
                statementArrayList.add(new ReturnStatement(function.getStatement().getBlock(), new IntegerValue(new Integer(0))));
            else
                statementArrayList.add(new ReturnStatement(function.getStatement().getBlock(), new FloatValue(new Float(0.0))));
        }
    }
}
