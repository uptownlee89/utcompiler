/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc.error;

import java_cup.runtime.ComplexSymbolFactory;
import java_cup.runtime.Symbol;
import utmcc.UTSymbolFactory;
import utmcc.sym;

/**
 * Created by uptown on 2014. 4. 6..
 */
public class ParseError extends Error {
    Symbol token;

    protected String type = "ParseError";


    public ParseError(String error, int line, int column, int length, String text, Symbol token) {
        super(error, new UTSymbolFactory.Location("line", line, column), new ComplexSymbolFactory.Location("line", line, column + length), text);
        this.token = token;
    }
    public ParseError(String error, ComplexSymbolFactory.Location start, ComplexSymbolFactory.Location end, String text, Symbol token) {
        super(error, start, end, text);
        this.token = token;
    }

    private String expected() {
        if (token != null && token instanceof UTSymbolFactory.SmartSymbol) {
            Symbol left = ((UTSymbolFactory.SmartSymbol) token).getLeftSymbol();
            if (left != null) {
                switch (left.sym) {
                    case sym.IF:
                        return "(";
                    default:
                        break;
                }
            }
            switch (token.sym) {
                case sym.ELSE:
                    return "if or other statement";
                default:
                    break;
            }
        }
        return "";
    }

    @Override
    public String toString() {
        return super.toString() + " expected: " + this.expected();
    }

}