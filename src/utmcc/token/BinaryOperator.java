/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc.token;

import utmcc.expression.Value;
import utmcc.sym;

/**
 * Created by uptown on 2014. 4. 12..
 */
public class BinaryOperator extends Operator {

    public BinaryOperator(int type) {
        super(type);
    }

    public String toString() {
        switch (this.type) {
            case sym.DIV:
                return "/";
            case sym.PLUS:
                return "+";
            case sym.TIMES:
                return "*";
            case sym.MINUS:
                return "-";
            case sym.LT:
                return "<";
            case sym.LTE:
                return "<=";
            case sym.GT:
                return ">";
            case sym.GTE:
                return ">=";
            case sym.EQUALS:
                return "==";
            case sym.DIFF:
                return "!=";
        }
        return "";
    }

    public Value eval(Value e0, Value e1) {

        switch (this.type) {
            case sym.DIV:
                return e0.divide(e1);
            case sym.PLUS:
                return e0.plus(e1);
            case sym.TIMES:
                return e0.times(e1);
            case sym.MINUS:
                return e0.minus(e1);
            case sym.GT:
                return e0.greaterThan(e1).not();
            case sym.GTE:
                return e0.greaterThan(e1).not().or(e0.equals(e1));
            case sym.LT:
                return e0.greaterThan(e1);
            case sym.LTE:
                return e0.greaterThan(e1).or(e0.equals(e1));
            case sym.EQUALS:
                return e0.equals(e1);
            case sym.DIFF:
                return e0.equals(e1).not();
        }
        return null;
    }

}
