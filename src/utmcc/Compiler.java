/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc;

import utmcc.cross.tmachine.TCode;
import utmcc.error.Error;
import utmcc.error.ParseError;
import utmcc.error.ScanError;

import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;

/**
 * Created by uptown on 2014. 4. 6..
 */
public class Compiler {

    protected Parser parser;
    protected Scanner scanner;
    protected Program program;

    public Compiler() {
        program = new Program();
    }

    static public Compiler compile(String cFile) throws Exception {

        Compiler compiler = new Compiler();
        UTSymbolFactory sf = new UTSymbolFactory();
        FileInputStream inputStream = new FileInputStream(cFile);
        Parser p = new Parser(new Scanner(new InputStreamReader(inputStream), sf, compiler), sf, compiler, false, true);
        try {
            p.parse();
            Validator validator = new Validator(compiler.getProgram());
            validator.validate();
        } catch (Error e) {
            compiler.getProgram().addError(e);
        } catch (Exception e) {
            System.out.println(e);
            e.printStackTrace();
        }

        Scanner sc = (Scanner) p.getScanner();
        for (ScanError err : sc.getErrors()) {
            compiler.getProgram().addError(err);
        }

        for (ParseError err : p.errors) {
            compiler.getProgram().addError(err);
        }


        if(compiler.getProgram().errors().size() == 0) {
            BufferedWriter out = new BufferedWriter(new FileWriter("tree.txt"));
            out.write(compiler.getProgram().toString());
            out.close();
            out = new BufferedWriter(new FileWriter("table.txt"));
            Block globalBlock = compiler.getProgram().getGlobalBlock();
            out.write(globalBlock.symbolTableToString());
            out.close();
            System.out.println("tree.txt and table.txt are created.");
        }


        TCode code = new TCode(compiler.getProgram());
        if (compiler.getProgram().errors().size() == 0) {
            BufferedWriter out = new BufferedWriter(new FileWriter("code.T"));
            out.write(code.toCode());
            out.close();
        }


        return compiler;

    }


    public Program getProgram(){return program;}
}
