/*
 * Copyright (c) 2014. Lee, Juyoung (lee.juyoung04@gmail.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package utmcc;

import utmcc.error.ScopeError;
import utmcc.expression.Expression;
import utmcc.expression.IntegerValue;
import utmcc.expression.Variable;
import utmcc.token.Type;

import java.util.ArrayList;

/**
 * Created by uptown on 2014. 4. 12..
 */
public class Block {
    public static int GLOBAL = 0, FUNCTION = 1, COMPOUND = 2;
    private Block parent;
    protected int type;
    protected String name;
    protected int paramNum = 0;
    protected int localVarNum = 0;
    ArrayList<String> symbolTableLookup;

    public ArrayList<SymbolTableEntry> getSymbolTable() {
        return symbolTable;
    }

    ArrayList<SymbolTableEntry> symbolTable;
    ArrayList<Block> children;


    public Block getParent(){return parent;}
    public int getType(){return type;}
    public String getName(){return name;}

    public int getLengthOfParameter(){ return paramNum; }
    public int getNumberOfLocalVariable(){ return localVarNum; }
    public Block(String name, Block parent) {
        this.name = name;
        this.parent = parent;
        this.symbolTable = new ArrayList<SymbolTableEntry>();
        this.symbolTableLookup = new ArrayList<String>();
        this.children = new ArrayList<Block>();
        this.type = Block.GLOBAL;
    }

    public Block(String name, Block parent, int type) {
        this.name = name;
        this.parent = parent;
        this.symbolTable = new ArrayList<SymbolTableEntry>();
        this.symbolTableLookup = new ArrayList<String>();
        this.children = new ArrayList<Block>();
        this.type = type;
    }

    public void declareVariable(Type type, Variable variable) throws ScopeError {

        if (this.symbolTableLookup.indexOf(variable.getIdentifier()) != -1)
            throw new ScopeError("Redeclaration of '" + variable.getIdentifier() + "'");
        this.symbolTableLookup.add(variable.getIdentifier());
        this.symbolTable.add(new SymbolTableEntry(type, variable, SymbolTableEntry.VARIABLE));
        localVarNum ++;

    }

    public void declareParameter(Type type, Parameter parameter) throws Exception {
        if (this.symbolTableLookup.indexOf(parameter.identifier.getIdentifier()) != -1)
            throw new ScopeError("Redeclaration of '" + parameter.identifier.getIdentifier() + "'");
        this.symbolTableLookup.add(parameter.identifier.getIdentifier());
        this.symbolTable.add(new SymbolTableEntry(type, parameter.identifier, SymbolTableEntry.PARAMETER));
        Expression index = parameter.identifier.getIndex();
        if(index != null){
            if (!(index instanceof IntegerValue))
                throw new Exception();
            paramNum += ((IntegerValue) index).getValue();
        }
        else{
            paramNum ++;
        }

    }

    public void assignVariable(String identifier, Expression expression){
    }

    public int getLevel(){
        if(parent == null)
            return 0;
        return parent.getLevel() + 1;
    }

    public String symbolTableToString(){
        if(symbolTable.size() > 0) {
            String ret = "\n" + this.getExpendedName() + "\n";
            ret += "type\t\tname\t\tindex\t\trole\n";
            for (SymbolTableEntry entry : symbolTable) {
                ret += (entry.type.equals(new Type(sym.INT))) ? "int  " : "float";
                ret += "\t\t";
                ret += entry.variable.getIdentifier() + "\t\t";
                ret += entry.variable.getIndex() != null ? entry.variable.getIndex() : "";
                ret += "\t\t";
                ret += entry.role == SymbolTableEntry.PARAMETER ? "parameter" : "variable";
                ret += '\n';
            }
            for (Block child : children) {
                ret += child.symbolTableToString();
            }
            return ret;
        }
        String ret = "";
        for (Block child : children) {
            ret += child.symbolTableToString();
        }
        return ret;
    }

    public Block createChild(String name, int type){
        Block ret = new Block(name, this, type);
        this.children.add(ret);
        return ret;
    }

    public SymbolTableEntry getVariable(String name) throws ScopeError{
        return getVariable(name, 0);
    }
    public SymbolTableEntry getVariable(String name, int index) throws ScopeError{
//        if(index > 1) {
//            if(parent != null) {
//                Block global = parent;
//                while (global.parent != null)
//                    global = global.parent;
//                return global.getVariable(name, 0);
//            }
//            else{
//                for(SymbolTableEntry entry : symbolTable){
//                    if(entry.getVariable().getIdentifier().equals(name)){
//                        return entry;
//                    }
//                }
//            }
//            throw new ScopeError("'" + name + "' is not visible identifier.");
//        }
        for(SymbolTableEntry entry : symbolTable){
            if(entry.getVariable().getIdentifier().equals(name)){
                return entry;
            }
        }
        if(parent != null){
            return parent.getVariable(name, ++index);
        }
        throw new ScopeError("'" + name + "' is not visible identifier.");
    }

    public String getExpendedName(){
        String thisName;
        if(this.type == Block.GLOBAL){
            thisName = "GLOBAL";
        }
        else if(this.type == Block.FUNCTION){
            thisName = "FUNCTION(" + name + ")";
        }
        else {
            thisName = name;
            if(!name.startsWith("DEFAULT")) {
                int order = this.parent.getOrder(this);
                thisName += "(" + order + ")";
            }
        }
        if(parent != null)
            return parent.getExpendedName() + " - " + thisName;
        return thisName;
    }

    public int getOrder(Block child){
        String name = child.name;
        int order = 0;
        for(Block b : this.children){
            if(b == child) break;
            if(b.name.equals(name)){
                order ++;
            }
        }
        return order;
    }

    public ArrayList<Block> getChildren(){return children;}

    public Block findChild(int type, String name){
        for(Block block : children){
            if(block.type == type && block.name.equals(name)){
                return block;
            }
        }
        return null;
    }



//    public SymbolTableEntry


//    public Variable findIdentifier(String identifier){
//
//    }

}
